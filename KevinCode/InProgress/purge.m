%Some of Rodrigo's scripts like to change the default font and font size.
%I personally like to keep the defaults as they are. This script will reset
%the defaults back the way God intended.


set(0,'defaultAxesFontSize',11)
set(0,'defaultAxesFontName','Helvetica')