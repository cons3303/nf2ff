
clc
close all
clear all


filename = 'G:\My Drive\ARRC\NF2FF\nf2ff\KevinCode\InProgress\2019-01-22_20_47_48_00_V_UniformUniform.csv';
delimiter = ',';
formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';
fileID = fopen(filename,'r');

dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string',  'ReturnOnError', false);
fclose(fileID);

% Convert the contents of columns containing numeric text to numbers.
% Replace non-numeric text with NaN.
raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = mat2cell(dataArray{col}, ones(length(dataArray{col}), 1));
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));


% Split data into numeric and string columns.
rawNumericColumns = {};
rawStringColumns = string(raw(:, [1,2,3,4,5,6,7,8,9,10,11]));


% Create output variable
data = raw;

% Clear temporary variables
clearvars filename delimiter formatSpec fileID dataArray ans raw col numericData rawNumericColumns rawStringColumns;

for i = 1:length(data)
    temp = string(data(i,1));
    key(i,1) = contains(temp,"HEADER");
end
key = double(key); 
numSections = sum(key);


count = 1;
 for i = 1: length(key)
     if key(i) == 1
         x(count) = data(i+1,5);
         y(count) = data(i+1,6);
         amp(count) = data(i+23,4);
         phase(count) = data(i+23,5);
         count = count+1;
     end
 end
 
x = double(string(x))';
y = double(string(y))';
amp = double(string(amp))';
phase = double(string(amp))';

finished(:,1) = x;
finished(:,2) = y;
finished(:,3) = amp;
finished(:,4) = phase;

