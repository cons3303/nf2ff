clc
close all
clear all

filename = 'sampleH.csv';
fileID = fopen(filename);
formatSpec = '%s %s %s %s %s %s %s %s %s';
N = 10;
header = textscan(fileID,formatSpec,N,'Delimiter',',');
fclose(fileID);

data = csvread('sampleH.csv',3,0);

T = table(data);