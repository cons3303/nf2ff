% x = coTable.x(rows);
% y = coTable.y(rows);
% 
% this = sqrt((y(17) - y(1))^2+(x(17)-x(1))^2);
% this = [x j*y]
% figure
% plot(x,y,'o')
clc
close all
clear all

totalx = 15*28e-3;
x = linspace(-totalx/2,totalx/2,16);
[gridx,gridyy] = meshgrid(x,x);

% this = 1:10
% that = 10:11
% 
% [x,y] = meshgrid(this,that);