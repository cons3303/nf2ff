%dataFile = csvread('/Volumes/My Passport/2018 Characterization Data from Pei',0,0)
% HEADER,TR Address, RF Direction,Select Command, Phase Shift (deg), Attenuation (dB), Temperature (C),Date,Time,
% ,0,Rx,Vertical,0.000,0.000,0.0,2018/06/18,10:31:21
% Freq(Hz),S11(DB),S11(DEG),S12(DB),S12(DEG),S21(DB),S21(DEG),S22(DB),S22(DEG)

%Polarization
%  V == 0
%  H == 1

clear all
close all
clc


numlines =101;
origin = datevec(datetime('1970/01/01','Format','yyyy/MM/dd'));

%initialize
ctr = 1;
ctr_files = 1;
ctr_read_lines = 0;
%first directory
starting = cd;
%cd('/Volumes/My Passport/2018 Characterization Data from Pei');
% cd('E:\2018 Characterization Data from Pei');
dataFile = fopen('2019-01-22_21_59_46_00_H_UniformUniform.csv')
cd(starting)

%Get total number of lines, 54525745 lines for -10dBm
tStart = datetime('now')
% % n = 0;
% % tline = fgetl(dataFile);
% % while ischar(tline)
% %   tline = fgetl(dataFile);
% %   n = n+1;
% % end
%f = waitbar(ctr/54525745*100,'Loading')
tStartImport = datetime('now');
while ~feof(dataFile)
    tic;
    
    channel       = []; %0-64
    polarization  = []; %0-1
    phsState      = []; %5.625*n - n=0...63
    ampState      = []; %0.5*n - n=0...63
    temperature   = []; 
    dates         = [];
    
    freq          = [];
    amplitude     = []; %db
    phase         = []; %deg
    
    while toc < 0.05 %ensure low computation time
        %first headers
        format = ',%d %s %s  %f  %f  %.1f %{yyyy/MM/dd}D %*s';
        ExcData = textscan(dataFile,format,2,'Delimiter',',','HeaderLines',1);
        % to get the freq and S params
        format = '%u64 %f %f %f %f %f %f %f %f' ;
        SData = textscan(dataFile,format,numlines+1,'Delimiter',',','HeaderLines',1);
        
        pol = ~strcmp('Vertical',ExcData{3});
        %rearrange
        tic;
        channel(ctr_read_lines + [1:numlines])        = repmat(ExcData{1},numlines,1);%[channel;      repmat(ExcData{1},numlines,1)];
        polarization(ctr_read_lines + [1:numlines])   = repmat(pol,numlines,1);%[polarization; repmat(pol       ,numlines,1)];
        phsState(ctr_read_lines + [1:numlines])       = repmat(ExcData{4},numlines,1);%[phsState;     repmat(ExcData{4},numlines,1)];
        ampState(ctr_read_lines + [1:numlines])       = repmat(ExcData{5},numlines,1);%[ampState;     repmat(ExcData{5},numlines,1)];
        %temperature((end+1):(end+1+numlines))    = repmat(ExcData{6},numlines,1);%[temperature;  repmat(ExcData{6},numlines,1)];
        dates(ctr_read_lines + [1:numlines])       = ...
            repmat(etime(datevec(ExcData{7}),origin),numlines,1);%[dates;        repmat(ExcData{7},numlines,1)];
        
        freq(ctr_read_lines + [1:numlines])           = [SData{1}];
        amplitude(ctr_read_lines + [1:numlines])      = [SData{4}];
        phase(ctr_read_lines + [1:numlines])          = [SData{5}];
        
        
        %counter
        ctr/54525745*100
        %waitbar(ctr/54525745,'Loading');
        ctr = ctr+numlines;
        ctr_read_lines = ctr_read_lines + numlines;
    end
    tStartSaveWorkspace(ctr_files) = datetime('now')
    
    %Save data so far
    strFilename = strcat('results',num2str(ctr_files),'.mat')
    save(strFilename)
    clearvars channel polarization phsState ampState temperature dates ...
              freq amplitude phase
    
    ctr_files = ctr_files + 1;
    ctr_read_lines = 0;
end
fclose(dataFile);
tEnd = datetime('now')

%% put all them back
    Channel       = [];
    Polarization  = [];
    PhsState      = [];
    AmpState      = [];
    Temperature   = [];
    Dates         = [];
    
    Freq          = [];
    Amplitude     = [];
    Phase         = [];
for i=1:26%ctr_files
    clearvars channel polarization phsState ampState temperature dates ...
              freq amplitude phase
    strFilename = strcat('results',num2str(i),'.mat')
    load(strFilename)
    
    Channel = [Channel channel];
    Polarization = [Polarization polarization]; 
    PhsState     = [PhsState phsState];
    AmpState     = [AmpState ampState];
    Dates     = [Dates dates];
    Freq     = [Freq freq];
    Phase     = [Phase phase];
    Amplitude     = [Amplitude amplitude];
    
end


%% SAVE Data

    data.channel       = Channel; %0-63
    data.polarization  = Polarization; %0-1
    data.phsState      = PhsState; %5.625*n - n=0...63
    data.ampState      = AmpState; %0.5*n - n=0...63
    %data.temperature   = Temperature; 
    data.dates         = Dates;
    
    data.freq          = Freq;
    data.amplitude     = Amplitude; %db
    data.phase         = Phase; %deg

save('APARCharData+3dBm.mat','Channel', 'Polarization', 'PhsState', 'AmpState', 'Temperature', 'Dates' ...
,'Freq', 'Amplitude', 'Phase')
