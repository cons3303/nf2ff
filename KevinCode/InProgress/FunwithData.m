clc
close all
clear all

tic
%Whatever freauency you want. I supposed you could just loop for all freqs
freq = 5.3450e9;

%For H
filename = '2019-01-22_21_59_46_00_H_UniformUniform.csv';
coTable = ImportMMwave(filename);
filename = '2019-01-22_20_47_48_00_V_UniformUniform.csv';
cxTable = ImportMMwave(filename);

rows = (coTable.FreqHz == freq);

co(:,1) = coTable.x(rows);
co(:,2) = coTable.y(rows);
co(:,3) = coTable.S12DB(rows);
co(:,4) = coTable.S12DEG(rows);

cross(:,1) = cxTable.x(rows);
cross(:,2) = cxTable.y(rows);
cross(:,3) = cxTable.S12DB(rows);
cross(:,4) = cxTable.S12DEG(rows);

FFparams.pei = true;
FFparams.freq = freq;
FFparams.nbr_samples = 361;
FFparams.H_angle_range = 120;
FFparams.V_angle_range = 120;

[FF] = nf2ffFunction(FFparams,co,cross);
FF.coordinate = 'L3';

kplot(FF);
toc
% save('coNew.mat','co');