clc
close all
clear all

load('peico.mat'); cross = finished; clear finished;
load('peicx.mat'); co = finished; clear finished;
load('FFparams.mat');
FFparams.pei = true;
FFparams.nbr_samples = 361;
FF = nf2ffFunction(FFparams,co,cross); close all;
FF.title = char("Pei's Data");
kplot(FF);