clc
close all
clear all

load('coNew.mat'); 
load('peicx.mat');
load('FFparams.mat');

FFparams.pei = true;
[FF] = nf2ffFunction(FFparams,co,finished);
% close all;


FF.coordinate = 'L3';
FF.title = 'Far-Field Patterns of NCAR 8x8';
kplot(FF);
saveas(gcf,'FF.png');



