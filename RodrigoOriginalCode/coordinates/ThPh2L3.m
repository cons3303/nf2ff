%[EL3co,EL3x] = ThPh2L3(TH,PH,ETH,EPH)
%To L3 definition
function [EL3co,EL3x] = ThPh2L3(TH,PH,ETH,EPH)
EL3co = cosd(PH).*ETH -sind(PH).*EPH;
EL3x  = sind(PH).*ETH +cosd(PH).*EPH;
end