%[U,V,W] = ThPh2UVW(TH,PH)
%TH,PH pairs are meshgrid type (deg)
%U,V,W defined by Masters et al. (also in Balanis)
function [U,V,W] = ThPh2UVW(TH,PH)
U = sind(TH).*cosd(PH);
V = sind(TH).*sind(PH);
W = cosd(TH);
end

