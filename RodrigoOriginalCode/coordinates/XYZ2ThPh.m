% [ETH,EPH] = XYZ2ThPh(TH,PH,EX,EY,EZ)
% ETh,EPh to Ex,Ey,Ey - I deduced this formulas from Masters et al.
function [ETH,EPH] = XYZ2ThPh(TH,PH,EX,EY,EZ)
ETH = EX.*cosd(TH).*cosd(PH) + EY.*cosd(TH).*sind(PH) -EZ.*sind(TH);
EPH = -EX.*sind(PH) + EY.*cosd(PH);
end