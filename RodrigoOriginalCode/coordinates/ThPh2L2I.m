% [EL2IAZ,EL2IEL] = ThPh2L2I(TH,PH,ETH,EPH)
% Th,Ph to L2-I (Az/El)
function [EL2IAZ,EL2IEL] = ThPh2L2I(TH,PH,ETH,EPH)
norm = 1./sqrt(1-(sind(TH)).^2.*(sind(PH)).^2);
EL2IAZ = (cosd(PH).*ETH         -cosd(TH).*sind(PH).*EPH).*norm;
EL2IEL  = (cosd(TH).*sind(PH).*ETH         +cosd(PH).*EPH).*norm;
end
