% [EX,EY,EZ] = ThPh2L2I(TH,PH,ETH,EPH)
% ETh,EPh to Ex,Ey,Ey - I deduced this formulas from Masters et al.
function [EX,EY,EZ] = ThPh2XYZ(TH,PH,ETH,EPH)
EX =  cosd(TH).*cosd(PH).*ETH   -sind(PH).*EPH;
EY =  cosd(TH).*sind(PH).*ETH   +cosd(PH).*EPH;
EZ = -sind(TH).*ETH;
end