% [ETH,EPH] = L2I2ThPh(TH,PH,EL2IAZ,EL2IEL)
% Th,Ph to L2-I (Az/El)
function [ETH,EPH] = L2I2ThPh(TH,PH,EL2IAZ,EL2IEL)
norm = 1./sqrt(1-(sind(TH)).^2.*(sind(PH)).^2);
ETH = (cosd(PH).*EL2IAZ         +cosd(TH).*sind(PH).*EL2IEL).*norm;
EPH  = (-cosd(TH).*sind(PH).*EL2IAZ         +cosd(PH).*EL2IEL).*norm;
end