%[EL,AZ] = UVW2ElAz(U,V,W)
%U,V,W  meshgrid type
%EL AZ defined by Masters et al. 
function [EL,AZ] = UVW2ElAz(U,V,W)
AZ = asind(U);
EL = asind(V./cosd(AZ));
end