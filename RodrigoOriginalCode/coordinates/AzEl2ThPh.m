%[TH,PH] = AzEl2ThPh(AZ,EL)
%AZ,EL pairs are meshgrid type
%AZ/EL defined by Masters et al.
function [TH,PH] = AzEl2ThPh(AZ,EL)
   [U,V,W]=AzEl2UVW(AZ,EL);
   [TH,PH] = UVW2ThPh(U,V,W);

% Another method - works
%    [U,V] = AzEl2UV(AZ,EL);
%    [TH,PH] = UV2ThPh(U,V);
%    
%    U2 = cosd(EL).*sind(AZ);
%    V2 = sind(EL);
%    
%    %TH and PH
%    PH2 = atan2(V2,U2)*180/pi;
%    TH2 = asind(V2./sind(PH2));
%    %issue when PH = 0, then
%    index = isnan(TH2);%
%    TH2(index) = abs(AZ(index));
%    
%    TH-TH2
end

