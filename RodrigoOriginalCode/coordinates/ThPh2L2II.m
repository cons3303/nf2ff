% [EL2IIalpha,EL2IIepsilon] = ThPh2L2II(TH,PH,ETH,EPH)
% Th,Ph to L2-II (El/Az) 
%alpha is the azimuth component
%epsilon is the elevation component
function [EL2IIalpha,EL2IIepsilon] = ThPh2L2II(TH,PH,ETH,EPH)
norm = 1./sqrt(1-(sind(TH)).^2.*(cosd(PH)).^2);
EL2IIalpha = (cosd(TH).*cosd(PH).*ETH         -sind(PH).*EPH).*norm;
EL2IIepsilon  = (sind(PH).*ETH         +cosd(TH).*cosd(PH).*EPH).*norm;
end