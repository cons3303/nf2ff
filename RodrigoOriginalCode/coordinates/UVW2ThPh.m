%[TH,PH] = UV2ThPh(AZ,EL)
%U,V pairs are meshgrid type
%Th Ph defined by Masters et al. (also in Balanis)
function [TH,PH] = UVW2ThPh(U,V,W)
PH = atan2(V,U)*180/pi;
TH = asind(V./sind(PH));
%issue when PH = 0, then 
index = isnan(TH);%
TH(index) = acosd(W(index));
%TH(index) = abs(AZ(index));
end

