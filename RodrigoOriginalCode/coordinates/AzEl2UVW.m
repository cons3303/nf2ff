%[U,V] = AzEl2UV(AZ,EL)
%AZ,EL pairs are meshgrid type
%AZ/EL defined by Masters et al.
function [U,V,W] = AzEl2UV(AZ,EL)
U = cosd(EL).*sind(AZ);
V = sind(EL);
W = cosd(EL).*cosd(AZ);
end

