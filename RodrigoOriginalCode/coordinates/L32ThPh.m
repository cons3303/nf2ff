%[ETH,EPH] = L32ThPh(TH,PH,EL3H,EL3V)
%To L3 definition - ELco=EL3H is horizontal 
function [ETH,EPH] = L32ThPh(TH,PH,EL3H,EL3V)
ETH = cosd(PH).*EL3H +sind(PH).*EL3V;
EPH  =-sind(PH).*EL3H +cosd(PH).*EL3V;
end