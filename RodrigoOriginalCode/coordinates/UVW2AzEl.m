%[AZ,EL] = UVW2AzEl(U,V,W)
%U,V,W  meshgrid type
%AZ EL defined by Masters et al. 
function [AZ,EL] = UVW2AzEl(U,V,W)
EL = asind(V);
AZ = asind(U./cosd(EL));
end

