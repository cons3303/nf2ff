%[U,V,W] = ElAz2UV(elevation,azimuth)
%AZ,EL pairs are meshgrid type
%EL/AZ defined by Masters et al.
function [U,V,W] = ElAz2UVW(EL,AZ)
U = sind(AZ);
V = cosd(AZ).*sind(EL);
W = cosd(EL).*cosd(AZ);
end

