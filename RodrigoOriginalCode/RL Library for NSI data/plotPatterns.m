%plot 2D  Co with El and Az cuts plus the 2D X pol normalized
%[figHandler] = plotPatterns(AZ,EL,PatternCmplx_Co,PatternCmplx_X,cutAngleOptions)
%cutAngleOptions is an array with possible locations for the EL and AZ cuts
function [figHandler] = plotPatterns(AZ,EL,PatternCmplx_Co,PatternCmplx_X,cutAngleOptions,autoscale)
%% PLOTTING

if nargin >= 5
    options = cutAngleOptions;
else
    options = -45:5:45;
end

if nargin <= 5
    autoscale = 1;

end


Pattern2plot_Co = PatternCmplx_Co;
Pattern2plot_X = PatternCmplx_X;

%get norm (MAX) val
normCo = max(max(Pattern2plot_Co));
normX = max(max(Pattern2plot_X));
norm = max([normCo normX]);
if normCo > normX
index2norm = Pattern2plot_Co==norm;
else
    index2norm = Pattern2plot_X==norm;
end

%2D plot - Co
figure
subplot(2,2,3)
surf(AZ,EL,db(Pattern2plot_Co/norm))
%colorbar
shading interp
axis square
caxis([-50 0])
if autoscale
    xlim([-65 65])
    ylim([-65 65])
end
xlabel('Azimuth (deg)')
ylabel('Elevation (deg)')
view(2)

%2D plot - X
subplot(2,2,2)
surf(AZ,EL,db(Pattern2plot_X/norm))
colorbar
shading interp
axis square
caxis([-50 0])
if autoscale
    xlim([-65 65])
    ylim([-65 65])
end
xlabel('Azimuth (deg)')
ylabel('Elevation (deg)')
view(2)




%fetch for cut angles

normElAngle = EL(index2norm);
normAzAngle = AZ(index2norm);
[angleErrorEl, index2ElAngle] = min(abs(options-normElAngle));
[angleErrorAz, index2AzAngle] = min(abs(options-normAzAngle));
targetElAngle = options(index2ElAngle)
targetAzAngle = options(index2AzAngle)

%AZ cut
[~, index2EL]= min(abs(EL(:)-targetElAngle));
index2plot  = EL == EL(index2EL(1));
h1 =subplot(2,2,1)
hold on
plot(AZ(index2plot),db(Pattern2plot_Co(index2plot)/norm))
plot(AZ(index2plot),db(Pattern2plot_X(index2plot)/norm),'--')
if autoscale
    xlim([-65 65])
end
ylim([-50 0])
xlabel('Azimuth (deg)')
ylabel('Amplitude(dB)')
grid on
grid minor
axis square
%ylim([-65 65])

%EL cut
[~, index2Az]= min(abs(AZ(:)-targetAzAngle));
index2plot  = AZ == AZ(index2Az(1));
h1 =subplot(2,2,4)
%set(h1, 'Ydir', 'reverse')
%set(h1, 'YAxisLocation', 'Right')
hold on
plot(db(Pattern2plot_Co(index2plot)/norm),EL(index2plot))
plot(db(Pattern2plot_X(index2plot)/norm),EL(index2plot),'--')
if autoscale
    ylim([-65 65])
end
xlim([-50 0])
ylabel('Elevation (deg)')
xlabel('Amplitude(dB)')
grid on
grid minor
axis square

figHandler = gcf;
end