%% Function to plot values for CO-Az-Mag cut
%  patternArray Input data dimension is (361,361,6)
%  ElAz = 'El','Az'
%  CoX = 'Co','X'
%  MagPhs ='Mag','Phs'
%  optional 'Nrm' to normalize
%  example 
%  cut = getCut(Uniform_3_V{1},'Co','Phs','Az','Nrm');
function toPlot = plotCut(patternArray,CoX,MagPhs,varargin)
    Azimuth = patternArray(:,:,1);
    Elevation = patternArray(:,:,2);
    
    %get array to plot
    if nargin == 4
        toPlot = getCut(patternArray,CoX,MagPhs,varargin{1});
    elseif nargin == 5
        toPlot = getCut(patternArray,CoX,MagPhs,varargin{1},varargin{2});
    end
   
       
   %plot
   if strcmp(varargin{1},'Az')
        x = Azimuth(find(Elevation == 0));
       plot(x,toPlot)
       xlabel('Azimuth (deg)')
   elseif strcmp(varargin{1},'El')
       x = Elevation(find(Azimuth == 0));
       plot(x,toPlot)
       xlabel('Elevation (deg)')
   end
   
   %get Mag of Phs
   if strcmp(MagPhs,'Phs')
       ylabel ('Phase (deg)');
   else
       ylabel ('Magnitude (dB)');
   end
end