%% Function to get the lobes positions and  values
% getLobes(x,curve) or getLobes(curve) / curve should be 1D
% output will be a struct 
% out.MainLobePos/out.MainVal
% out.RightLobePos/out.RightLobeVal
% out.LeftLobePos/out.eftLobeVal
function out = getLobes(varargin)
    if nargin == 1
        curve = varargin{1};
        x = 1:numel(curve);
    elseif nargin ==2
        x = varargin{1};
        curve = varargin{2};
    end

    [peaks_vals,indx_peaks_array] = findpeaks(curve);
    %Identify the max among peaks
    [out.MainLobeVal,indx_maxPeak] = max(peaks_vals);
     out.MainLobeIndx = indx_peaks_array(indx_maxPeak);
     out.MainLobePos = x(out.MainLobeIndx);
    %Get left side lobe
    if indx_maxPeak ~= 1 
        out.LeftLobeIndx = indx_peaks_array(indx_maxPeak - 1);
        out.LeftLobeVal = curve(out.LeftLobeIndx);
        out.LeftLobePos = x(out.LeftLobeIndx);
    else
        out.LeftLobePos = NaN;
        out.LeftLobeVal = NaN;
    end
    %Get right side lobe
    if indx_maxPeak ~= numel(indx_peaks_array) 
        out.RightLobeIndx = indx_peaks_array(indx_maxPeak + 1);
        out.RightLobeVal = curve(out.RightLobeIndx);
        out.RightLobePos = x(out.RightLobeIndx);
    else
        out.RightLobePos = NaN;
        out.RightLobeVal = NaN;
    end
    
    % get position if an x arrary is given

end
