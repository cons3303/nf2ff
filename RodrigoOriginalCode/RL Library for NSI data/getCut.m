%% Function to get values for CO-Az-Mag cut -------------------------------
%  patternArray: Input data dimension is (361,361,6)
%  CoX = 'Co','X'
%  MagPhs ='Mag','Phs'
%  ElAz = 'El','Az','D'
%  optional 'Nrm' to normalize - local normalization only
% example 
%  cut = getCut(Uniform_3_V{1},'Co','Phs','Az','Nrm');
function [x, y] = getCut(patternArrayOrStruct,CoX,MagPhs,varargin)
    if isstruct(patternArrayOrStruct)
        patternArray = patternArrayOrStruct.data;
    else
        patternArray = patternArrayOrStruct;
    end

    Azimuth = patternArray(:,:,1);
    Elevation = patternArray(:,:,2);

   %get Co or X, (361,361,6)->(361,361) 
   maxAt3 = max(max(patternArray(:,:,3)));
   maxAt5 = max(max(patternArray(:,:,5)));
   if maxAt3 > maxAt5
       indx_Co = 3;
       indx_X  = 5;
   else
       indx_Co = 5;
       indx_X  = 3;
   end
   
   %Co or X
   if strcmp(CoX,'Co')
      index_to_extract = indx_Co;
   else
       index_to_extract = indx_X;
   end
   
   
   
   %get Mag of Phs
   if strcmp(MagPhs,'Phs')
       index_to_extract = index_to_extract + 1;
   end
   
   %get cut
   if ischar(varargin{1})
        array361 = patternArray(:,:,index_to_extract);
       if strcmp(varargin{1},'Az')
           indx_cut = find(Elevation == 0);
           cut = array361(indx_cut);
           x = Azimuth(indx_cut);
       elseif strcmp(varargin{1},'El')
           indx_cut = find(Azimuth == 0);
           cut = array361(indx_cut);
           x = Elevation(indx_cut);
       elseif strcmp(varargin{1},'D')
           indx_cut = find(Azimuth == Elevation);
           cut = array361(indx_cut);
           x = Azimuth(indx_cut);
       end
   end
   
   %normalize
   if nargin >4
       if (  strcmp(varargin{2},'Nrm') & strcmp(MagPhs,'Mag'))
           array2D_Co = patternArray(:,:,indx_Co);
           nrm = max(array2D_Co(indx_cut));
           cut = cut - nrm;
       end
   end
   
   if nargout == 2
       x = x;
       y = cut;
   elseif nargout == 1
       x = cut;
   end
   
end
