%input are data struct and indexes to fetch
%the function fetches the values for indexes, and then return a data struct
%containing only the fetched values
function [dataTrimmed] = trimDataSet(data, indxs) 

    dataTrimmed.channel       = data.channel(indxs);
    dataTrimmed.polarization  = data.polarization(indxs); %0-1
    dataTrimmed.phsState      = data.phsState(indxs); %5.625*n - n=0...63 -> 0-63
    dataTrimmed.ampState      = data.ampState(indxs); %0.5*n - n=0...63 -> 0-63minAtt
    %data.temperature   = Temperature; 
    %dataTrimmed.dates         = dataTrimmed.dates;
    
    dataTrimmed.freq          = data.freq(indxs); %GHz
    dataTrimmed.amplitude     = data.amplitude(indxs); %db
    dataTrimmed.phase         = data.phase(indxs); %deg
    dataTrimmed.repetition    = data.repetition(indxs);
    dataTrimmed.complex       = data.complex(indxs);
end