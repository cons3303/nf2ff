% [figHandler] = plot2DCOandX(AZ,EL,PatternCmplx_meas_Co,PatternCmplx_meas_X,PatternCmplx_comp_Co,PatternCmplx_comp_X)
function [figHandler] = plot2DCOandX(AZ,EL,PatternCmplx_meas_Co,PatternCmplx_meas_X,PatternCmplx_comp_Co,PatternCmplx_comp_X)
%% PLOTTING
figHandler = figure('Units','Normalized','Position',[0.1 0.1 0.7 0.7])
    
for i=1:2 %first plot computed then plot measured
    if i==1
        Pattern2plot_Co = PatternCmplx_comp_Co;
        Pattern2plot_X = PatternCmplx_comp_X;
    elseif i==2
        Pattern2plot_Co = PatternCmplx_meas_Co;
        Pattern2plot_X = PatternCmplx_meas_X;
    end
    
    norm = max(max(Pattern2plot_Co));
    
    %2D plot - Co
    subplot(2,2,1+(i-1)*2)
    surf(AZ,EL,db(Pattern2plot_Co/norm))
    colorbar
    %colorbar
    shading interp
    axis square
    caxis([-60 0])
    xlim([-65 65])
    ylim([-65 65])
    xlabel('Azimuth (deg)')
    ylabel('Elevation (deg)')
    view(2)
    if i==1
        title('COMPUTED Pattern - Co')
    elseif i==2
        title('MEASURED Pattern - Co')
    end

    %2D plot - X
    subplot(2,2,2+(i-1)*2)
    surf(AZ,EL,db(Pattern2plot_X/norm))
    colorbar
    shading interp
    axis square
    caxis([-60 0])
    xlim([-65 65])
    ylim([-65 65])
    xlabel('Azimuth (deg)')
    ylabel('Elevation (deg)')
    view(2)
    if i==1
        title('COMPUTED Pattern - X')
    elseif i==2
        title('MEASURED Pattern - X')
    end

end
end