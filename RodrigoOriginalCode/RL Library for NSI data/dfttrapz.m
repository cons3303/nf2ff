%[TDFT] = dfttrapz(x,fx,Nbr_freqs,neg)
% x=x or time samples,fx=function(x),Nbr_freqs=+/-frequency number points
%it works for fixed sampled data dt=const or dx=const
%x,fx must be 1-D array
%the frequency vector k = m*dk, can be 
% Nbr_freqs<0   m = -M/2:M/2-1 
% Nbr_freqs>0   m = 0:M-1 
function [TDFT] = dfttrapz(x,fx,Nbr_freqs)
N = numel(x);  %number of samples
M = abs(Nbr_freqs); %frequencies number

Xstart = x(1);

n = (0:(N-1)); %indexes for samples
if Nbr_freqs > 0     
    m = (0:(M-1));
else 
    m = -M/2:M/2-1;
end

dx = (x(2)-Xstart);
dk = 2*pi/M/dx;
%w or k=2*pi*f samples
k = m*dk;
%f samples
f = fx;
difference = f(3:end) - f(1:(end-2));
df = [f(2)-f(1)  difference/2 f(end)-f(end-1)];
%df = [difference 0];


%dft
% for i=m
%     DFT(i+1) = exp(1i*k(i+1)*dx/2)*dx*sum(f.*exp(1i*k(i+1)*x));
%     dDFT(i+1) = exp(1i*k(i+1)*dx/2)*dx*sum(df.*exp(1i*k(i+1)*x));
% end

if k(1) < 0 %negative frequencies too
    FFT =  conj(exp(-1i*k*(-2+dx/2))*dx.*fftshift(fft(f,M)));
    dFFT = conj(exp(-1i*k*(-2+dx/2))*dx.*fftshift(fft(df,M)));
else
    FFT =  conj(exp(-1i*k*(Xstart+dx/2))*dx.*fft(f,M));
    dFFT = conj(exp(-1i*k*(Xstart+dx/2))*dx.*fft(df,M));
end
%tdft
th = k*dx/2;
TDFT = sinc(th/pi).*FFT + 1./(1i*2*th).*(exp(1i.*th)-sinc(th/pi)).*dFFT;