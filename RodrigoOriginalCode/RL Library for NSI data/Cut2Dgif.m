%CREATE GIF

%Find Normalization value
for  i=1:nbr_patterns
    peaksCo(i) = max(max(getCut2D(toPlot{i},'Co','Mag')));
end
nrm = max(peaksCo);
    
mkdir(gifFilename)
%Co surface
for i=1:nbr_patterns
    figure(1)
    Co2D = getCut2D(toPlot{i},'Co','Mag') - nrm;
    %get rid of looooow vals
    Co2D = (Co2D < -50).*lowlimdB + (Co2D > -50).*Co2D;
    %plot
    surf(Azimuth,Elevation,Co2D,'linestyle','none','FaceColor','interp');
    set(gcf,'color','none')
    view(0,90)
    caxis([lowlimdB 0])
    colorbar
    
    drawnow
    frame = getframe(1);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    if i == 1;
        imwrite(imind,cm,strcat(gifFilename,'/',gifFilename,'Co.gif'),'gif', 'Loopcount',inf,'DelayTime',.2);
    else
        imwrite(imind,cm,strcat(gifFilename,'/',gifFilename,'Co.gif'),'gif','WriteMode','append','DelayTime',.2);
    end
end

%X surface
for i=1:nbr_patterns
    figure(2)
    Co2D = getCut2D(toPlot{i},'X','Mag') - nrm;
    %get rid of looooow vals
    Co2D = (Co2D < -50).*lowlimdB + (Co2D > -50).*Co2D;
    %plot
    surf(Azimuth,Elevation,Co2D,'linestyle','none','FaceColor','interp');
    set(gcf,'color','none')
    view(0,90)
    caxis([lowlimdB 0])
    colorbar
    
     drawnow
    frame = getframe(2);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    if i == 1;
        imwrite(imind,cm,strcat(gifFilename,'/',gifFilename,'X.gif'),'gif', 'Loopcount',inf,'DelayTime',.2);
    else
        imwrite(imind,cm,strcat(gifFilename,'/',gifFilename,'X.gif'),'gif','WriteMode','append','DelayTime',.2);
    end
end
