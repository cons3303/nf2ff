%% Function to get values for CO or X 2D surface
%  patternArray Input data dimension is (361,361,6)
%  CoX = 'Co','X'
%  MagPhs ='Mag','Phs'
function cut2D = getCut2D(patternArrayOrStruct,CoX,MagPhs)
   if isstruct(patternArrayOrStruct)
      patternArray = patternArrayOrStruct.data;
   else
       patternArray = patternArrayOrStruct;
   end
   %get Co or X, (361,361,6)->(361,361) 
   maxAt3 = max(max(patternArray(:,:,3)));
   maxAt5 = max(max(patternArray(:,:,5)));
   if maxAt3 > maxAt5
       indx_Co = 3;
       indx_X  = 5;
   else
       indx_Co = 5;
       indx_X  = 3;
   end
   
   %Co or X
   if strcmp(CoX,'Co')
      index_to_extract = indx_Co;
   else
       index_to_extract = indx_X;
   end
   
   %get Mag of Phs
   if strcmp(MagPhs,'Phs')
       index_to_extract = index_to_extract + 1;
   end
   
   
       cut2D = patternArray(:,:,index_to_extract);
   
end
