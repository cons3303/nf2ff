%% [ETHrot,EPHrot] = rotatePattern(TH,PH,ETH,EPH,alfa,dowplot)
% Rotates a pattern given in ETH,EPH TH/PH coordinates around the z-axis
% by an angle alfa in deg (90 deg by default)

function [ETHrot,EPHrot] = rotatePattern(TH,PH,ETH,EPH,alfaDeg,doweplot)
if nargin == 4
    alfaDeg = 90;
    doweplot =0;
elseif nargin == 5
    dowplot = 0;
end


%Sampling point Coordinates from Pxyz to Px'y'z'(rotz90)
%TH,PH 2 U,V,W
[U,V,W] = ThPh2UVW(TH,PH);
%Rotation matrix
R = rotz(alfaDeg);
%U,V,W 2 U'V'W'
Pt = R*transp([U(:),V(:),W(:)]); %transformed 2D matrix to vectors
Ut = reshape(transp(Pt(1,:)),size(TH));
Vt = reshape(transp(Pt(2,:)),size(TH));
Wt = reshape(transp(Pt(3,:)),size(TH));
%U'V'W' 2 TH',El'
[THt,PHt] = UVW2ThPh(Ut,Vt,Wt);

%I believe a rotation in TH,PH should be even easier like
%THt = TH;
%PHt= PH + alfa;

%ROTATE FIELD VECTORS X,Y,Z 2 X'Y'W'
%ETh,EPh 2 EX,EY,EZ
[E_X,E_Y,E_Z] = ThPh2XYZ(TH,PH,ETH,EPH);
%X,Y,Z 2 X'Y'W'
Et = R*transp([E_X(:),E_Y(:),E_Z(:)]); %transformed 2D matrix to vectors
Et_X = reshape(transp(Et(1,:)),size(TH));
Et_Y = reshape(transp(Et(2,:)),size(TH));
Et_Z = reshape(transp(Et(3,:)),size(TH));
%X'Y'W' 2 TH',El'
[Et_TH,Et_PH] = XYZ2ThPh(THt,PHt,Et_X,Et_Y,Et_Z);
if doweplot
    [Et_L2I_AZ,Et_L2I_EL] = ThPh2L2I(THt,PHt,Et_TH,Et_PH);
    [AZ,EL] = UVW2AzEl(U,V,W);
    plotPatterns(AZ,EL,Et_L2I_AZ,Et_L2I_EL,[0]);
end
%PATTERN DATA
%resample so Ex and Ey are sampled at same coordinates
F_TH=TriScatteredInterp(THt(:), PHt(:), Et_TH(:));%TriScatteredInterp%scatteredInterpolant
Einterp_TH = F_TH(TH(:),PH(:));
ETHrot = reshape(transp(Einterp_TH),size(TH));

F_PH=TriScatteredInterp(THt(:), PHt(:), Et_PH(:));%TriScatteredInterp%scatteredInterpolant
Einterp_PH = F_PH(TH(:),PH(:));
EPHrot = reshape(transp(Einterp_PH),size(TH));
end