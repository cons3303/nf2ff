clear all
close all
clc


N = 80; %sample number
M = 256; %frequencies number

Xstart = -2;
Xstop = 2;

n = (0:(N-1)); %indexes for samples
%m = (0:(M-1));
m = -M/2:M/2-1;
dx = (Xstop-Xstart)/(N);
dk = 2*pi/M/dx;
%x samples
x = Xstart + n*dx;
%w or k=2*pi*f samples
k = m*dk;
%f samples
f = x.^2;
difference = f(3:end) - f(1:(end-2));
df = [f(2)-f(1)  difference/2 f(end)-f(end-1)];
%df = [difference 0];

%FT - analytical--------------
k2 = k(2:end);
FT = - (1i)*2*((2*k2.^2+2*1i*k2-1).*exp(2*1i*k2)+(-2*k2.^2+2*1i*k2+1).*exp(-2*1i*k2))./(k2.^3);

%DFT--------------
for i=0:M-1
    DFT(i+1) = exp(1i*k(i+1)*dx/2)*dx*sum(f.*exp(1i*k(i+1)*x));
    dDFT(i+1) = exp(1i*k(i+1)*dx/2)*dx*sum(df.*exp(1i*k(i+1)*x));
end

%FFT--------------
if k(1) < 0 %negative frequencies too
    FFT =  conj(exp(-1i*k*(-2+dx/2))*dx.*ifftshift(fft(f,M)));
    dFFT = conj(exp(-1i*k*(-2+dx/2))*dx.*ifftshift(fft(df,M)));
else
    FFT =  conj(exp(-1i*k*(Xstart+dx/2))*dx.*fft(f,M));
    dFFT = conj(exp(-1i*k*(Xstart+dx/2))*dx.*fft(df,M));
end
%TDFT--------------
th = k*dx/2;
%TDFT = sinc(th/pi).*DFT + 1./(1i*2*th).*(exp(1i.*th)-sinc(th/pi)).*dDFT;
TDFT = dfttrapz(x,f,-M);
%norm 
norm = 5.333;

figure
subplot(2,1,1)
plot(m(2:end),db(FT/norm))
hold on
plot(m,db(FFT/norm))
plot(m,db(DFT/norm))
plot(m,db(TDFT/norm))
legend('FT','FFT','DFT','TDFT')
xlim([m(1) m(end)])

subplot(2,1,2)
hold on
plot(m(2:end),angle(FT/norm)*180/pi)
plot(m,angle(FFT/norm)*180/pi)
plot(m,angle(DFT/norm)*180/pi)
plot(m,angle(TDFT/norm)*180/pi)
xlim([m(1) m(end)])

error = sum(abs(FT-TDFT(2:end)))