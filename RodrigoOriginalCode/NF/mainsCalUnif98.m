%% NF-FF transformation
clc
clear all
close all
set(0,'defaultAxesFontSize',17)
set(0,'defaultAxesFontName','Times')
%% Import NF data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif98 data/NFCalUnif98.txt';%Hpol CalUnif67 data
data = importNFfromfile(filename);
X=data(:,:,1)*0.0254;
Y=data(:,:,2)*0.0254;

NF_co_DB = data(:,:,3);
NF_co_Deg = data(:,:,4);
NF_co_cmplx = db2mag(NF_co_DB).*exp(1i*NF_co_Deg*pi/180);

NF_x_DB = data(:,:,5);
NF_x_Deg = data(:,:,6);
NF_x_cmplx = db2mag(NF_x_DB).*exp(1i*NF_x_Deg*pi/180);
%Define in AZ,EL (H,V or X,Y) convention
%pol=0 %means Co is V,EL or Y
%pol=1 %means Co is H,AZ or X
pol=1;

% % Import Probe data %we may delete this
% filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/ProbeEx/FFL2AzElPCProbeEx.txt';
% dataFF = importFFfromfile(filename,1);
% AZ_probe=dataFF(:,:,1);
% EL_probe=dataFF(:,:,2);
% 
% F_co_DB_probe = dataFF(:,:,3);
% F_co_Deg_probe = dataFF(:,:,4);
% F_co_cmplx_probe = db2mag(F_co_DB_probe).*exp(1i*F_co_Deg_probe*pi/180);
% 
% F_x_DB_probe = dataFF(:,:,5);
% F_x_Deg_probe = dataFF(:,:,6);
% F_x_cmplx_probe = db2mag(F_x_DB_probe).*exp(1i*F_x_Deg_probe*pi/180);

%% Input parameters
% FF grid definition
%nbr of samples
param.freq = 5.35e9;
linewidth = 2;
FF.nbr_samples = 361;
FF.H_angle_range = 130;
FF.V_angle_range = 130;

%% Simple derivations
lambda = 3e8/param.freq;
[Nx, My] = size(X);
NI = 128; %nbr freqs in y
MI = 128; %nbr freqs in x
dx = abs(X(1,2)-X(1,1));
dy = abs(Y(2,1)-Y(1,1));


%% Process NF2FF
%create grid of FF sample angles --------------
H_angles = linspace(-FF.H_angle_range/2,FF.H_angle_range/2,FF.nbr_samples);
V_angles = linspace(-FF.V_angle_range/2,FF.V_angle_range/2,FF.nbr_samples);

[AZ,EL] = meshgrid(H_angles,V_angles);

[U,V,W] = AzEl2UVW(AZ,EL);
[TH,PH] = UVW2ThPh(U,V,W);

%Compute FF --------------
K = 2*pi/lambda;
m = -MI/2:1:(MI/2-1);
n = -NI/2:1:(NI/2-1); %from balanis
[M,N] = meshgrid(m,n);
Kx = 2*K*M/MI;
Ky = 2*K*N/NI;
Kz = sqrt(K^2 - Kx.^2 - Ky.^2);

%DFT -----------
%deltas for trapezoidal approximation NARASIMHAM 1984

for i=1:NI
    for j=1:MI
        Fco(i,j) = sum(sum(NF_co_cmplx.*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
        Fcross(i,j)  = sum(sum(NF_x_cmplx .*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
    end
end

%FFT ------------
%Fco= ifftshift(ifft2(NF_co_cmplx,NI,MI));%fliplr(flipud(fftshift(fft2(NF_co_cmplx,NI,MI))));
%Fx = ifftshift(ifft2(NF_x_cmplx,NI,MI));%fliplr(flipud(fftshift(fft2(NF_x_cmplx,NI,MI))));

Fco_Interp = interp2(Kx, Ky, Fco,K*U,K*V,'spline');
Fcross_Interp = interp2(Kx, Ky, Fcross,K*U,K*V,'spline');
if pol==1 %H,AZ,X pol
    F_AZ = Fco_Interp;
    F_EL = Fcross_Interp;
elseif pol==0 %V,EL or Y
    F_AZ = Fcross_Interp;
    F_EL = Fco_Interp;
end

%PROBE CORRECTION ---------------
%get probe spectrum (or pattern, I am not sure yet what NSI gives you)
% [E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,E_L2I_AZ_ProbeEy,E_L2I_EL_ProbeEy]...
%     = getProbePatterns(TH,PH,1);
[E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,E_L2I_AZ_ProbeEy,E_L2I_EL_ProbeEy]...
    = getWR187Patterns(TH,PH,1);

%I rather define the patterns in H,V or X,Y or AZ,EL rather than Co and X
Fx_probeEx = E_L2I_AZ_ProbeEx;
Fy_probeEx = E_L2I_EL_ProbeEx;
Fx_probeEy = E_L2I_AZ_ProbeEy;
Fy_probeEy = E_L2I_EL_ProbeEy;
%Correction - from slater
denominator = Fx_probeEx.*Fy_probeEy - Fx_probeEy.*Fy_probeEx;
F_AZ_corr = (F_AZ.*Fy_probeEy - F_EL.*Fy_probeEx)./denominator;
F_EL_corr = (-F_AZ.*Fx_probeEy + F_EL.*Fx_probeEx)./denominator;

%Spectrum to FF fields
EL2IAZ = F_AZ_corr.*cosd(TH);
EL2IEL = F_EL_corr.*cosd(TH);

%Tranform to other polarization definitions
[ETH,EPH] = L2I2ThPh(TH,PH,EL2IAZ,EL2IEL);
[EL3H,EL3V] = ThPh2L3(TH,PH,ETH,EPH);
%% PLOT NF DATA
figure
surf(X,Y,(NF_co_DB))
shading interp
colorbar
view(2)
xlabel('X(m)')
ylabel('Y(m)')
title('NF - Co')

figure
surf(X,Y,(NF_x_DB))
shading interp
colorbar
view(2)
xlabel('X(m)')
ylabel('Y(m)')
title('NF - X')
%% PLOT COMPUTED PATTERNS

if pol == 0 %Vertical pol
    EL3co = EL3V;
    EL3x = EL3H;
    %L2I
    EL2Ico = EL2IEL;
    EL2Ix = EL2IAZ;
else
    EL3co = EL3H;
    EL3x = EL3V;    
    %L2I
    EL2Ico = EL2IAZ;
    EL2Ix = EL2IEL;
end
    [figHandler] = plotPatterns(AZ,EL,EL3co,EL3x,[0])
    suptitle('L3')
    [figHandler] = plotPatterns(AZ,EL,EL2Ico,EL2Ix,[0])
    suptitle('L2I')


% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(FTH),(FPH));
% quiver3(U,V,W,EX,EY,EZ)
% xlabel('x-axis')
% ylabel('y-axis')

% %THPH coordinates
% unit = ones(FF.nbr_samples,FF.nbr_samples);
% zero = zeros(FF.nbr_samples,FF.nbr_samples);
% uTH = unit;
% uPH = unit;
% figure
% pbaspect([1 1 1])
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(uTH),(zero));
% quiver3(U,V,W,EX,EY,EZ,'red')
% hold on
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,zero,uPH)
% quiver3(U,V,W,EX,EY,EZ,'blue')
% 
% %L3 coordinates
% unit = ones(FF.nbr_samples,FF.nbr_samples)*5;
% zero = zeros(FF.nbr_samples,FF.nbr_samples);
% uL3co = unit;
% uL3x = unit;
% figure
% pbaspect([1 1 1])
% [uTH,uPH] = L32ThPh(TH,PH,uL3co,zero);
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(uTH),(uPH));
% quiver3(U,V,W,EX,EY,EZ,'red')
% hold on
% [uTH,uPH] = L32ThPh(TH,PH,zero,uL3x);
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(uTH),(uPH));
% quiver3(U,V,W,EX,EY,EZ,'blue')


% %L3 Watch in componentes
% figure
% [ETHfromL3,EPHfromL3] = L32ThPh(TH,PH,EL3co,zeros(FF.nbr_samples,FF.nbr_samples));
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(ETHfromL3),(EPHfromL3));
% quiver3(U,V,W,EX,EY,EZ)
% figure
% [ETHfromL3,EPHfromL3] = L32ThPh(TH,PH,zeros(FF.nbr_samples,FF.nbr_samples),EL3x);
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(ETHfromL3),(EPHfromL3));
% quiver3(U,V,W,EX,EY,EZ)



%% Compare

%% COMPARE WITH MEASURED DATA - PROBE CORRECTION EVALUATION

filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif98 data/FFL3PCCalUnif98.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,1);
AZ_meas=dataFF(:,:,1);
EL_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
F_co_PC_meas = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
F_x_PC_meas = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);


F_co_PC_meas_interp = interp2(AZ_meas, EL_meas, F_co_PC_meas,AZ,EL,'spline');
F_x_PC_meas_interp = interp2(AZ_meas, EL_meas, F_x_PC_meas,AZ,EL,'spline');

%for plotting --------------
F_meas_co = F_co_PC_meas_interp;
F_meas_cross = F_x_PC_meas_interp;
F_comp_co = EL3co;
F_comp_cross = EL3x;
X2plot_meas = AZ_meas;
Y2plot_meas = EL_meas;
X2plot_comp = AZ;
Y2plot_comp = EL;
%plot parameters
ymin = -60;
ymax = 0;
Xcut = -45.5;%45.5; %where to cut the data
Ycut = 45.5;%39; %where to cut the data

%Plotting --------- no user input needed
    plot2DCOandX(AZ,EL,F_meas_co,F_meas_cross,F_comp_co,F_comp_cross) 
    suptitle('Ludwig 3, Probe Corrected - Computed from NF data')
%norms
norm_meas = max(max(F_meas_co));
norm_Interp = max(max(F_comp_co));

%Cut in Kx/K or U
figure
subplot(2,1,1)
hold on
indxs = Y2plot_meas==Ycut;
plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
indxs = Y2plot_comp==Ycut;
plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Azimuth')
ylabel('Magnitude(dB)')
legend('L3 meas','L3 from NF')
grid on
grid minor
%Cut in Ky/K or V
subplot(2,1,2)
hold on
indxs = X2plot_meas==Xcut;
plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
indxs = X2plot_comp==Xcut;
plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Elevation')
ylabel('Magnitude(dB)')
grid on
grid minor


    
%     strFileName2D = strcat(strFolderPath,'2DPatternID',num2str(n),...
%         'Az',num2str(targetAzAngle),...
%         'El',num2str(targetElAngle),...
%         'Pol',pol(n));
%     strFileNameCuts = strcat(strFolderPath,'CutsPattern',num2str(n),...
%         'Az',num2str(targetAzAngle),...
%         'El',num2str(targetElAngle),...
%         'Pol',pol(n));
    %saveas(figH_2D,strFileName2D,'png')
    %saveas(figH_Cuts,strFileNameCuts,'png')

