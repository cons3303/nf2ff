%% Import Probe data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/FFL2AzElProbeEx.txt';
dataFF = importFFfromfile(filename);
AZ_probe=dataFF(:,:,1);
EL_probe=dataFF(:,:,2);

F_co_DB_probe = dataFF(:,:,3);
F_co_Deg_probe = dataFF(:,:,4);
F_co_cmplx_probe = db2mag(F_co_DB_probe).*exp(1i*F_co_Deg_probe*pi/180);

F_x_DB_probe = dataFF(:,:,5);
F_x_Deg_probe = dataFF(:,:,6);
F_x_cmplx_probe = db2mag(F_x_DB_probe).*exp(1i*F_x_Deg_probe*pi/180);
%% Plot
%plot FF imported
[figHandler] = plotPatterns(AZ,EL,F_co_cmplx_probe,F_x_cmplx_probe,[0])