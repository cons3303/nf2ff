%% EXTRACT PROBE PATTERNS IN L2 AZ/EL DEFINITION
%[E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx] = getWR187Patterns(TH,PH,doweplot)
% Needs the:
%  - sampling grid given in TH,PH meshgrid format
%  - plot=0->no plot, plot=1->plot patterns
%Right now it exports the pattern of a probe in a DEFINED FREQUENCY
%OBSERVATION: I am yet to determine if what I am importing is an spectrum
%or the actual FF pattern
function [E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,E_L2I_AZ_ProbeEy,E_L2I_EL_ProbeEy] = getWR187Patterns(TH,PH,doweplot)

%% Import FF WR187 data in Ludwig2 AZ/EL definition
if nargin ==0
    clear all
    close all
    param.freq = 5.35e9;
    linewidth = 2;
    FF.nbr_samples = 361;
    FF.H_angle_range = 130;
    FF.V_angle_range = 130;
    doweplot=1;
    H_angles = linspace(-FF.H_angle_range/2,FF.H_angle_range/2,FF.nbr_samples);
    V_angles = linspace(-FF.V_angle_range/2,FF.V_angle_range/2,FF.nbr_samples);
    
    [AZ,EL] = meshgrid(H_angles,V_angles);
    
    [U,V,W] = AzEl2UVW(AZ,EL);
    [TH,PH] = UVW2ThPh(U,V,W);
    
    
else
    %[U,V,W] = ThPh2UVW(TH,PH);
    [U,V,W] = ThPh2UVW(TH,PH);
    [AZ,EL] = UVW2AzEl(U,V,W);
end

linewidth = 2;
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/WR187/WR187-SphericalV15/Freq5/PCWR187-SphericalV15_2D_Co_and_X.csv';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,1);
AZ_meas=dataFF(:,:,1);
EL_meas=dataFF(:,:,2);

E_co_DB_file = dataFF(:,:,3);
E_co_Deg_file = dataFF(:,:,4);
E_co_ProbeEx_file = db2mag(E_co_DB_file).*exp(1i*E_co_Deg_file*pi/180);

E_x_DB_file = dataFF(:,:,5);
E_x_Deg_file = dataFF(:,:,6);
E_x_ProbeEx_file = db2mag(E_x_DB_file).*exp(1i*E_x_Deg_file*pi/180);
%PATTERN DATA ----------------
E_L3_H_ProbeEx = interp2(AZ_meas, EL_meas, E_co_ProbeEx_file,AZ,EL,'spline');
E_L3_V_ProbeEx = interp2(AZ_meas, EL_meas, E_x_ProbeEx_file,AZ,EL,'spline')*db2mag(0);
% TRANSFORM TO OTHER COORDINATES
%Theta & phi
[E_TH_probeEx,E_PH_probeEx]=L32ThPh(TH,PH, E_L3_H_ProbeEx, E_L3_V_ProbeEx);
%L2AL/EL
[E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx]=ThPh2L2I(TH,PH, E_TH_probeEx,E_PH_probeEx);
[Ex_probeEx,Ey_probeEx,Ez_probeEx] =ThPh2XYZ(TH,PH, E_TH_probeEx, E_PH_probeEx);

if doweplot
    %for plotting --------------
    F_meas_co = E_L2I_AZ_ProbeEx;
    F_meas_cross = E_L2I_EL_ProbeEx;
    F_comp_co = ones(size(E_L2I_AZ_ProbeEx))*1e-30;
    F_comp_cross = ones(size(E_L2I_AZ_ProbeEx))*1e-30;
    X2plot_meas = U;
    Y2plot_meas = V;
    X2plot_comp = U;
    Y2plot_comp = V;
    %plot parameters
    ymin = -60;
    ymax = 0;
    Xcut = 0; %where to cut the data
    Ycut = 0; %where to cut the data
    
    %norms
    norm_meas = max(max(F_meas_co));
    norm_Interp = max(max(F_comp_co));
    %Cut in Kx/K or U
    figure
    subplot(2,1,1)
    hold on
    indxs = Y2plot_meas==Ycut;
    plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    indxs = Y2plot_comp==Ycut;
    plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('AZ (deg)')
    ylabel('Magnitude(dB)')
    legend('L2AZ/EL meas')
    grid on
    grid minor
    %Cut in Ky/K or V
    subplot(2,1,2)
    hold on
    indxs = X2plot_meas==Xcut;
    plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    indxs = X2plot_comp==Xcut;
    plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('EL (deg)')
    ylabel('Magnitude(dB)')
    grid on
    grid minor
    suptitle('L2 FF - Probe Ex')
    
    
    %plot cartesian components
   plotPatterns(AZ,EL,E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,[0])
   suptitle('Probe Ex- L2 AZ/EL')
    % figure
    % surf(U,V,db(Ex_probeEx))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ex - Probe Ex')
    % %Ey
    % figure
    % surf(U,V,db(Ey_probeEx))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ey - Probe Ex')
end
%% Pattern for Ey components - assuming 90 deg rotation

%Sampling point Coordinates from Pxyz to Px'y'z'(rotz90)
%Az,El 2 U,V,W
[U,V,W] = AzEl2UVW(AZ,EL);
%Rotation matrix
R = rotz(90);
%U,V,W 2 U'V'W'
Pt = R*transp([U(:),V(:),W(:)]); %transformed 2D matrix to vectors
Ut = reshape(transp(Pt(1,:)),size(AZ));
Vt = reshape(transp(Pt(2,:)),size(AZ));
Wt = reshape(transp(Pt(3,:)),size(AZ));
%U'V'W' 2 Az',El'
[AZt,ELt] = UVW2AzEl(Ut,Vt,Wt);
[Tht,PHt] = UVW2ThPh(Ut,Vt,Wt);

%ROTATE FIELD VECTORS X,Y,Z 2 X'Y'W'
%ETh,EPh 2 EX,EY,EZ
[E_X_probeEx,E_Y_probeEx,E_Z_probeEx] = ThPh2XYZ(TH,PH,E_TH_probeEx,E_PH_probeEx);
%X,Y,Z 2 X'Y'W'
Et = R*transp([E_X_probeEx(:),E_Y_probeEx(:),E_Z_probeEx(:)]); %transformed 2D matrix to vectors
Et_X_probeEx = reshape(transp(Et(1,:)),size(AZ));
Et_Y_probeEx = reshape(transp(Et(2,:)),size(AZ));
Et_Z_probeEx = reshape(transp(Et(3,:)),size(AZ));
%X'Y'W' 2 Az',El'
[Et_TH_probeEx,Et_PH_probeEx] = XYZ2ThPh(Tht,PHt,Et_X_probeEx,Et_Y_probeEx,Et_Z_probeEx);
[Et_L2I_AZ_ProbeEx,Et_L2I_EL_ProbeEx] = ThPh2L2I(Tht,PHt,Et_TH_probeEx,Et_PH_probeEx);
%plotPatterns(AZt,ELt,Et_L2I_EL_ProbeEx,Et_L2I_AZ_ProbeEx,[0]);

%PATTERN DATA
%resample so Ex and Ey are sampled at same coordinates
F_AZ=TriScatteredInterp(AZt(:), ELt(:), Et_L2I_AZ_ProbeEx(:));%TriScatteredInterp%scatteredInterpolant
Einterp_L2I_AZ_ProbeEy = F_AZ(AZ(:),EL(:));
E_L2I_AZ_ProbeEy = reshape(transp(Einterp_L2I_AZ_ProbeEy),size(AZ));

F_EL=TriScatteredInterp(AZt(:), ELt(:), Et_L2I_EL_ProbeEx(:));
Einterp_L2I_EL_ProbeEy = F_EL(AZ(:),EL(:));
E_L2I_EL_ProbeEy = reshape(transp(Einterp_L2I_EL_ProbeEy),size(AZ));

% TRANSFORM TO OTHER COORDINATES
%Theta & phi
[E_TH_probeEy,E_PH_probeEy]=L2I2ThPh(TH,PH, E_L2I_AZ_ProbeEy, E_L2I_EL_ProbeEy);
%L2AZEL
[E_L3_H_ProbeEy,E_L3_V_ProbeEy]=ThPh2L3(TH,PH, E_TH_probeEy,E_PH_probeEy);
[Ex_probeEy,Ey_probeEy,Ez_probeEy] =ThPh2XYZ(TH,PH, E_TH_probeEy, E_PH_probeEy);
%THE ACTUAL CORRECTION FACTORS
if doweplot
    %for plotting --------------
    F_meas_co = E_L2I_EL_ProbeEy;
    F_meas_cross = E_L2I_AZ_ProbeEy;
    F_comp_co = ones(size(E_L2I_AZ_ProbeEx))*1e-30;
    F_comp_cross = ones(size(E_L2I_AZ_ProbeEx))*1e-30;
    X2plot_meas = AZ;
    Y2plot_meas = EL;
    X2plot_comp = AZ;
    Y2plot_comp = EL;
    %plot parameters
    ymin = -45;
    ymax = 0;
    Xcut = 0; %where to cut the data
    Ycut = 0; %where to cut the data
    
    %norms
    norm_meas = max(max(F_meas_co));
    norm_Interp = max(max(F_comp_co));
    %Cut in Kx/K or U
    figure
    subplot(2,1,1)
    hold on
    indxs = Y2plot_meas==Ycut;
    plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    indxs = Y2plot_comp==Ycut;
    %plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    %plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('U')
    ylabel('Magnitude(dB)')
    legend('L2AZ/EL meas')
    grid on
    grid minor
    %Cut in Ky/K or V
    subplot(2,1,2)
    hold on
    indxs = X2plot_meas==Xcut;
    plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    indxs = X2plot_comp==Xcut;
    %plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    %plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('V')
    ylabel('Magnitude(dB)')
    grid on
    grid minor
    suptitle('L2AZ/EL FF - Probe Ey ')

    %plot cartesian components
    %Ex
    %plot cartesian components
   plotPatterns(AZ,EL,F_meas_co,F_meas_cross,[0]);
   suptitle('L2 AZ/EL')
    % figure
    % surf(U,V,db(Ex_probeEy))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ex - Probe Ey')
    % %Ey
    % figure
    % surf(U,V,db(Ey_probeEy))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ey - Probe Ey')
end