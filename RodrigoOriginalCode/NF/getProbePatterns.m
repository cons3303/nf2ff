%% EXTRACT PROBE PATTERNS IN L2 AZ/EL DEFINITION
%[E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx] = getProbePatterns(TH,PH,doweplot)
% Needs the:
%  - sampling grid given in TH,PH meshgrid format
%  - plot=0->no plot, plot=1->plot patterns
%Right now it exports the pattern of a probe in a DEFINED FREQUENCY
%OBSERVATION: I am yet to determine if what I am importing is an spectrum
%or the actual FF pattern
function [E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,E_L2I_AZ_ProbeEy,E_L2I_EL_ProbeEy] = getProbePatterns(TH,PH,doweplot)

%% Import FF probeEx data in Ludwig2 AZ/EL definition
[U,V,W] = ThPh2UVW(TH,PH);

linewidth = 2;
filename = 'G:\My Drive\ARRC\NF2FF\nf2ff\NF\ProbeEx\FFL2AzELKxKyPCProbeEx.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,3);
U_meas=dataFF(:,:,1);
V_meas=dataFF(:,:,2);

E_co_DB_file = dataFF(:,:,3);
E_co_Deg_file = dataFF(:,:,4);
E_co_ProbeEx_file = db2mag(E_co_DB_file).*exp(1i*E_co_Deg_file*pi/180);

E_x_DB_file = dataFF(:,:,5);
E_x_Deg_file = dataFF(:,:,6);
E_x_ProbeEx_file = db2mag(E_x_DB_file).*exp(1i*E_x_Deg_file*pi/180);

%PATTERN DATA ----------------
E_L2I_AZ_ProbeEx = interp2(U_meas, V_meas, E_co_ProbeEx_file,U,V,'spline');
E_L2I_EL_ProbeEx = interp2(U_meas, V_meas, E_x_ProbeEx_file,U,V,'spline');
% TRANSFORM TO OTHER COORDINATES
%Theta & phi
[E_TH_probeEx,E_PH_probeEx]=L2I2ThPh(TH,PH, E_L2I_AZ_ProbeEx, E_L2I_EL_ProbeEx);
%L2AZEL
[E_L3_H_ProbeEx,E_L3_V_ProbeEx]=ThPh2L3(TH,PH, E_TH_probeEx,E_PH_probeEx);
[Ex_probeEx,Ey_probeEx,Ez_probeEx] =ThPh2XYZ(TH,PH, E_TH_probeEx, E_PH_probeEx);

if doweplot
    %for plotting --------------
    F_meas_co = E_L2I_AZ_ProbeEx;
    F_meas_cross = E_L2I_EL_ProbeEx;
    F_comp_co = E_L2I_EL_ProbeEx;
    F_comp_cross = E_L2I_EL_ProbeEx;
    X2plot_meas = U;
    Y2plot_meas = V;
    X2plot_comp = U;
    Y2plot_comp = V;
    %plot parameters
    ymin = -60;
    ymax = 0;
    Xcut = 0; %where to cut the data
    Ycut = 0; %where to cut the data
    
    %norms
    norm_meas = max(max(F_meas_co));
    norm_Interp = max(max(F_comp_co));
    %Cut in Kx/K or U
    figure
    subplot(2,1,1)
    hold on
    indxs = Y2plot_meas==Ycut;
    plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    indxs = Y2plot_comp==Ycut;
    plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('U')
    ylabel('Magnitude(dB)')
    legend('L2AZ/EL meas','L2AZ/EL from NF')
    grid on
    grid minor
    %Cut in Ky/K or V
    subplot(2,1,2)
    hold on
    indxs = X2plot_meas==Xcut;
    plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    indxs = X2plot_comp==Xcut;
    plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('V')
    ylabel('Magnitude(dB)')
    grid on
    grid minor
    suptitle('L3 FF - Probe Ex')
    
    
    %plot cartesian components
%    plotPatterns(AZ,EL,E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,[0])
%    suptitle('Probe Ex- L2 AZ/EL')
    % figure
    % surf(U,V,db(Ex_probeEx))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ex - Probe Ex')
    % %Ey
    % figure
    % surf(U,V,db(Ey_probeEx))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ey - Probe Ex')
end
%% Import FF probe data Ey
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/ProbeEy/FFL2AzELKxKyPCProbeEy.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,3);
U_meas=dataFF(:,:,1);
V_meas=dataFF(:,:,2);

E_co_DB_file = dataFF(:,:,3);
E_co_Deg_file = dataFF(:,:,4);
E_co_ProbeEy_file = db2mag(E_co_DB_file).*exp(1i*E_co_Deg_file*pi/180);

E_x_DB_file = dataFF(:,:,5);
E_x_Deg_file = dataFF(:,:,6);
E_x_ProbeEy_file = db2mag(E_x_DB_file).*exp(1i*E_x_Deg_file*pi/180);

%PATTERN DATA
E_L2I_AZ_ProbeEy = interp2(U_meas, V_meas, E_x_ProbeEy_file,U,V,'spline');
E_L2I_EL_ProbeEy = interp2(U_meas, V_meas, E_co_ProbeEy_file,U,V,'spline');
% TRANSFORM TO OTHER COORDINATES
%Theta & phi
[E_TH_probeEy,E_PH_probeEy]=L2I2ThPh(TH,PH, E_L2I_AZ_ProbeEy, E_L2I_EL_ProbeEy);
%L2AZEL
[E_L3_H_ProbeEy,E_L3_V_ProbeEy]=ThPh2L3(TH,PH, E_TH_probeEy,E_PH_probeEy);
[Ex_probeEy,Ey_probeEy,Ez_probeEy] =ThPh2XYZ(TH,PH, E_TH_probeEy, E_PH_probeEy);
%THE ACTUAL CORRECTION FACTORS

if doweplot
    %for plotting --------------
    F_meas_co = E_L2I_EL_ProbeEy;
    F_meas_cross = E_L2I_AZ_ProbeEy;
    F_comp_co = E_L2I_AZ_ProbeEy;
    F_comp_cross = E_L2I_AZ_ProbeEy;
    X2plot_meas = U;
    Y2plot_meas = V;
    X2plot_comp = U;
    Y2plot_comp = V;
    %plot parameters
    ymin = -60;
    ymax = 0;
    Xcut = 0; %where to cut the data
    Ycut = 0; %where to cut the data
    
    %norms
    norm_meas = max(max(F_meas_co));
    norm_Interp = max(max(F_comp_co));
    %Cut in Kx/K or U
    figure
    subplot(2,1,1)
    hold on
    indxs = Y2plot_meas==Ycut;
    plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    indxs = Y2plot_comp==Ycut;
    plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('U')
    ylabel('Magnitude(dB)')
    legend('L2AZ/EL meas','L2AZ/EL from NF')
    grid on
    grid minor
    %Cut in Ky/K or V
    subplot(2,1,2)
    hold on
    indxs = X2plot_meas==Xcut;
    plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
    plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
    indxs = X2plot_comp==Xcut;
    plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
    plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
    ylim([ymin ymax])
    xlabel('V')
    ylabel('Magnitude(dB)')
    grid on
    grid minor
    suptitle('L3 FF - Probe Ey ')

    %plot cartesian components
    %Ex
    %plot cartesian components
%    plotPatterns(AZ,EL,E_L2I_AZ_ProbeEy,E_L2I_EL_ProbeEy,[0])
%    suptitle('Probe Ey - L2 AZ/EL')
    % figure
    % surf(U,V,db(Ex_probeEy))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ex - Probe Ey')
    % %Ey
    % figure
    % surf(U,V,db(Ey_probeEy))
    % shading interp
    % view(2)
    % xlabel('U')
    % ylabel('V')
    % title('Ey - Probe Ey')
end