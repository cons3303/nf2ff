% *********************** NF-FF TRANSFORMATION ****************************
% Developer: Alessio Mancini, PhD student at CSU, NCAR collaborator.
% email: alessio@rams.colostate.edu
% Adviser: V. Chandra
% Supervisor at NCAR: Jorge Salazar
%
% Code main functions:
% - Load Real and Imaginary part of the Near Field from the files;
% - Parameters setting;
% - NF-FF transformation;
% - Applying Ludwig 3rd definition to the Directivity;
% - Probe correction;
%
% UPDATE 1: March 18.2014 (MAIN)
% UPDATE 2: March 28.2014 (PROBE CORRECTION)
% UPDATE 3: April 14.2014 (PROBE CORRECTION) - Final Version 04/14/2014
% *************************************************************************
clear all; 
close all;
clc;
%% Import NF data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/NFCalUnif60.txt';
data = importNFfromfile(filename);
X=data(:,:,1)*0.0254;
Y=data(:,:,2)*0.0254;

E_co_DB = data(:,:,3);
E_co_Deg = data(:,:,4);
E_co_cmplx = db2mag(E_co_DB).*exp(1i*E_co_Deg*pi/180);

E_x_DB = data(:,:,5);
E_x_Deg = data(:,:,6);
E_x_cmplx = db2mag(E_x_DB).*exp(1i*E_x_Deg*pi/180);



%% ------------------------ PARAMETERS SETTING -----------------------------
c = 3*10^8;
f = 5.35*10^9;    % operating frequency
lambda = c/f;
k = 2*pi/lambda;

%dz = 0.125;     % meters (3*lambda - distance between the probe and the antenna)
L = 43.377*0.0254;       % meters (sanning window length)
r = 5*lambda/2;  % far field equation: 2*d^2/lamda (d=antenna size); 5 = factor
                 % element size = lamda

% num_rows is the length of the file that contain the data
% next = is the resolution (Labview variable) + 2. It represents the first
% number of the next row.
num_rows = 900;
next = 31;
% -------------------------------------------------------------------------

% ---------------- STAGE 0: load data from the files ----------------------
real_co = real(E_co_cmplx);   
imaginary_co = imag(E_co_cmplx);
real_x = real(E_x_cmplx);     
imaginary_x = imag(E_x_cmplx);

% % re-ordering rows:
% % the data is taken at only one-point frequency and they are saved in a
% % column vector;          
% count = 1;
% row_new = 1;
% col_new = 1;
% for row = 1 : num_rows
%     real_new_co(row_new,col_new) = real_co(row,1);
%     imaginary_new_co(row_new,col_new) = imaginary_co(row,1);
%     real_new_x(row_new,col_new) = real_x(row,1);
%     imaginary_new_x(row_new,col_new) = imaginary_x(row,1);
%     count = count + 1;
%     col_new = col_new + 1;
%     if count == next;
%         col_new = 1;
%         row_new = row_new + 1;
%         count = 1;
%     end
% end
% 
% real_final_co = real_new_co;
% imaginary_final_co = imaginary_new_co;
% real_final_x = real_new_x;
% imaginary_final_x = imaginary_new_x;
% for row = 1 : 2 : length(real_new_x)
%     pos2 = 1;
%     pos = length(real_new_x);
%     for c = 1 : length(real_new_x)        % columns re-ordering
%         real_final_co(row,pos2) = real_new_co(row,pos);
%         imaginary_final_co(row,pos2) = imaginary_new_co(row,pos);  
%         real_final_x(row,pos2) = real_new_x(row,pos);
%         imaginary_final_x(row,pos2) = imaginary_new_x(row,pos);
%         pos2 = pos2 + 1;
%         pos = pos - 1;
%     end
% end
% 
% % ---- phase computation
% complex_num_co = real_final_co + 1i*imaginary_final_co;
% ampl_co = abs(complex_num_co);
% complex_num_x = real_final_x + 1i*imaginary_final_x;
% ampl_x = abs(complex_num_x);
% [row,column] = size(complex_num_co);
% for r = 1 : row
%     for c = 1 : column
%         if real_final_co(r,c) < 0
%            phase_co(r,c) = atan(imaginary_final_co(r,c)/real_final_co(r,c)) + pi;
%         else
%            phase_co(r,c) = atan(imaginary_final_co(r,c)/real_final_co(r,c));
%         end
%         if real_final_x(r,c) < 0
%            phase_x(r,c) = atan(imaginary_final_x(r,c)/real_final_x(r,c)) + pi;
%         else
%            phase_x(r,c) = atan(imaginary_final_x(r,c)/real_final_x(r,c));
%         end
%     end
% end

nf_co = E_co_cmplx;
nf_x = E_x_cmplx;

% ------------------- STAGE I: Cartesian Grid Creation --------------------
[N,M] = size(nf_co);

% Cartesian grid
% it represents the quantization of the scanning window considering the
% number of samples taken and the total length of the squared scanning
% window (dx = dy)
x_resol = linspace(-L/2,L/2,N);
y_resol = linspace(-L/2,L/2,M);
[x_coord, y_coord] = meshgrid(x_resol, y_resol);
dx = abs(x_resol(1)) - abs(x_resol(2));
dy = abs(y_resol(1)) - abs(y_resol(2));

MI = 2^(ceil(log2(M)));
NI = 2^(ceil(log2(N)));
m = (-MI/2:1:MI/2-1);
n = (-NI/2:1:NI/2-1);
k_X_Rectangular=2*pi*m/(MI*dx);
k_Y_Rectangular=2*pi*n/(NI*dy);
[k_Y_Rectangular_Grid,k_X_Rectangular_Grid] = meshgrid(k_Y_Rectangular,k_X_Rectangular);
            
k_Z_Rectangular_Grid = sqrt(k^2-k_X_Rectangular_Grid.^2-k_Y_Rectangular_Grid.^2);
            
figure;
imagesc(k_X_Rectangular,k_Y_Rectangular,db(nf_co));
set(gca,'Fontsize',14')
xlabel('Kx (m^-^1)','Fontsize',14);
ylabel('Ky (m^-^1)','Fontsize',14);
colorbar('Fontsize',14');
title('Near Field Co-polarization - Amplitude (dB)','Fontsize',14)
figure;
imagesc(k_X_Rectangular,k_Y_Rectangular,angle(nf_co)*180/pi);
set(gca,'Fontsize',14')
xlabel('Kx (m^-^1)','Fontsize',14);
ylabel('Ky (m^-^1)','Fontsize',14);
colorbar('Fontsize',14');
title('Near Field Co-polarization - Phase (deg)','Fontsize',14)
figure;
imagesc(k_X_Rectangular,k_Y_Rectangular,db(nf_x));
set(gca,'Fontsize',14')
xlabel('Kx (m^-^1)','Fontsize',14);
ylabel('Ky (m^-^1)','Fontsize',14);
colorbar('Fontsize',14');
title('Near Field Cross-polarization - Amplitude (dB)','Fontsize',14)
figure;
imagesc(k_X_Rectangular,k_Y_Rectangular,angle(nf_x)*180/pi);
set(gca,'Fontsize',14')
xlabel('Kx (m^-^1)','Fontsize',14);
ylabel('Ky (m^-^1)','Fontsize',14);
colorbar('Fontsize',14');
title('Near Field Cross-polarization - Phase (deg)','Fontsize',14)

% --------------- STAGE II: NF --> FF transformation ----------------------
NF_X_Complex = nf_co;
NF_Y_Complex = nf_x;

dtheta = 0.01;
dphi = 0.01;
theta1 = (-pi/2 + dtheta : dtheta : pi/2 - dtheta);
phi1 = (0 + dphi : dphi : pi - dphi);
[theta,phi] = meshgrid(theta1,phi1);
index = find(theta == 0);

f_X_Rectangular = ifftshift(ifft2(NF_X_Complex,MI,NI));
f_Y_Rectangular = ifftshift(ifft2(NF_Y_Complex,MI,NI));
f_Z_Rectangular = -(f_X_Rectangular.*k_X_Rectangular_Grid + f_Y_Rectangular.*k_Y_Rectangular_Grid)./k_Z_Rectangular_Grid;

% -------------- STAGE III: Spherical Components Computation --------------    
f_X_Spherical = interp2(k_X_Rectangular, k_Y_Rectangular, abs(f_X_Rectangular'), k*sin(theta).*cos(phi),k*sin(theta).*sin(phi),'spline');
f_Y_Spherical = interp2(k_X_Rectangular, k_Y_Rectangular, abs(f_Y_Rectangular'), k*sin(theta).*cos(phi),k*sin(theta).*sin(phi),'spline');
f_Z_Spherical = interp2(k_X_Rectangular, k_Y_Rectangular, abs(f_Z_Rectangular'), k*sin(theta).*cos(phi),k*sin(theta).*sin(phi),'spline');

figure;
imagesc(k_X_Rectangular,k_Y_Rectangular,db(f_X_Rectangular));
set(gca,'Fontsize',14')
xlabel('Kx (m^-^1)','Fontsize',14);
ylabel('Ky (m^-^1)','Fontsize',14);
colorbar('Fontsize',14');
title('Fx - Amplitude (dB)','Fontsize',14)
figure;
imagesc(k_X_Rectangular,k_Y_Rectangular,db(f_Y_Rectangular));
set(gca,'Fontsize',14')
colorbar('Fontsize',14');
xlabel('Kx (m^-^1)','Fontsize',14);
ylabel('Ky (m^-^1)','Fontsize',14);
set(gca,'Fontsize',14')
title('Fy - Amplitude (dB)','Fontsize',14)

% Etheta and Ephi computation
C = 1i*(k*exp(-1i*k*r))/(2*pi*r);
Etheta = C*(f_X_Spherical.*cos(phi)+f_Y_Spherical.*sin(phi));
Ephi = C*(-f_X_Spherical.*sin(phi)+f_Y_Spherical.*cos(phi));

W = 1/(2*120*pi).*(Etheta.*conj(Etheta)+Ephi.*conj(Ephi));
U = (abs(Etheta).^2 + abs(Ephi).^2);

% Computation of the radiated power through numerical integration
e_theta = [1 4 repmat([2 4], 1, floor(length(theta(1,:))/2) - 1) 1];
e_phi = [1 4 repmat([2 4], 1, floor(length(phi(:,1))/2) - 1) 1];
P = dphi*dtheta*sum(sum(U.*(e_theta'*e_phi).*abs(sin(theta))))/9;

% Applying Ludwig 3rd Definition to the Directivity
D = 4*pi*U/P;
U_Co =    (abs(Etheta.*cos(phi) - Ephi.*sin(phi))).^2;  
U_Cross = (abs(Etheta.*sin(phi) + Ephi.*cos(phi))).^2;  
D_Co =  4*pi*U_Co/P;
D_Cross = 4*pi*U_Cross/P;

% ---------------------- STAGE IV: Probe Correction -----------------------
%[D_Co_corrected, D_Cross_corrected] = Probe_correction_square_root(D_Co,D_Cross);
D_Co_corrected = D_Co;
D_Cross_corrected = D_Cross;

figure;
imagesc(theta1*180/pi,phi1*180/pi,20*log10(abs(Ephi)));
set(gca,'Fontsize',14')
title(sprintf('f = %f GHz',f/1000000000),'Fontsize',14);
xlabel('Theta (deg)','Fontsize',14);
ylabel('Phi (deg)','Fontsize',14);
colorbar('Fontsize',14');
title('E\phi','Fontsize',14)
figure;
imagesc(theta1*180/pi,phi1*180/pi,20*log10(abs(Etheta)));
set(gca,'Fontsize',14')
xlabel('Theta (deg)','Fontsize',14);
ylabel('Phi (deg)','Fontsize',14);
colorbar('Fontsize',14');
title('E\theta','Fontsize',14)

% Rectangular Ludwig 3rd Definition
figure;
imagesc(theta1*180/pi,phi1*180/pi,10*log10(abs(D_Co_corrected)));
set(gca,'Fontsize',14')
xlabel('Theta (deg)','Fontsize',14);
ylabel('Phi (deg)','Fontsize',14);
colorbar('Fontsize',14');
title('Co-polarization component','Fontsize',14)
figure;
imagesc(theta1*180/pi,phi1*180/pi,10*log10(abs(D_Cross_corrected)));
set(gca,'Fontsize',14')
xlabel('Theta (deg)','Fontsize',14);
ylabel('Phi (deg)','Fontsize',14);
colorbar('Fontsize',14');
title('Cross-polarization component','Fontsize',14)

% Cuts from Ludwig 3rd Definition
figure;
%E plane
plot(theta1*180/pi,10*log10(abs(D_Co_corrected(1,:))),'b', 'Linewidth',3); hold on
plot(theta1*180/pi,10*log10(abs(D_Cross_corrected(1,:))),'--b', 'Linewidth',3); hold on
%D plane
plot(theta1*180/pi,10*log10(abs(D_Co_corrected(16,:))),'r', 'Linewidth',3); hold on
plot(theta1*180/pi,10*log10(abs(D_Cross_corrected(16,:))),'--r', 'Linewidth',3); hold on 
%H plane
plot(theta1*180/pi,10*log10(abs(D_Co_corrected(31,:))/max(D_Co_corrected(31,:))),'g', 'Linewidth',3); hold on
plot(theta1*180/pi,10*log10(abs(D_Cross_corrected(31,:))/max(D_Co_corrected(31,:))),'--g', 'Linewidth',3); 
set(gca,'Fontsize',14')
xlabel('Theta (deg)','Fontsize',14) 
ylabel('Magnitude (dB)','Fontsize',14);
legend('E-plane CO','E-plane X','D-plane CO','D-plane X','H-plane CO', 'H-plane X','Fontsize',14)
ylim([-40,0])
xlim([-90,90])
grid on
title('Corrected Patterns','Fontsize',14)

