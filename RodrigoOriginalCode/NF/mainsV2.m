%% NF-FF transformation
clc
clear all
close all
set(0,'defaultAxesFontSize',17)
set(0,'defaultAxesFontName','Times')
%% Import NF data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/NFCalUnif67.txt';
data = importNFfromfile(filename);
X=data(:,:,1)*0.0254;
Y=data(:,:,2)*0.0254;

E_co_DB = data(:,:,3);
E_co_Deg = data(:,:,4);
E_co_cmplx = db2mag(E_co_DB).*exp(1i*E_co_Deg*pi/180);

E_x_DB = data(:,:,5);
E_x_Deg = data(:,:,6);
E_x_cmplx = db2mag(E_x_DB).*exp(1i*E_x_Deg*pi/180);

% Import Probe data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/FFL2AzElProbeEx.txt';
dataFF = importFFfromfile(filename);
AZ_probe=dataFF(:,:,1);
EL_probe=dataFF(:,:,2);

F_co_DB_probe = dataFF(:,:,3);
F_co_Deg_probe = dataFF(:,:,4);
F_co_cmplx_probe = db2mag(F_co_DB_probe).*exp(1i*F_co_Deg_probe*pi/180);

F_x_DB_probe = dataFF(:,:,5);
F_x_Deg_probe = dataFF(:,:,6);
F_x_cmplx_probe = db2mag(F_x_DB_probe).*exp(1i*F_x_Deg_probe*pi/180);

%% Input parameters
% FF grid definition
%nbr of samples
param.freq = 5.35e9;
linewidth = 2;
FF.nbr_samples = 361;
FF.H_angle_range = 130;
FF.V_angle_range = 130;

%% Simple derivations
lambda = 3e8/param.freq;
[NI, MI] = size(X);
dx = abs(X(1,2)-X(1,1));
dy = abs(Y(2,1)-Y(1,1));


%% Process NF2FF


%create grid of FF sample angles --------------
H_angles = linspace(-FF.H_angle_range/2,FF.H_angle_range/2,FF.nbr_samples);
V_angles = linspace(-FF.V_angle_range/2,FF.V_angle_range/2,FF.nbr_samples);

[AZ,EL] = meshgrid(H_angles,V_angles);

[U,V,W] = AzEl2UVW(AZ,EL);
[TH,PH] = UVW2ThPh(U,V,W);

%condition the sampled data - 17-12a,b Balanis
% for i=1:MI
%     for j=1:NI
%         Eprime_co_cmplx(i,j) = sum(sum(E_co_cmplx.*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
%         Fx(i,j) = sum(sum(E_x_cmplx.*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
%     end
% end

% U2 = cosd(EL).*sind(AZ);
% V2 = sind(EL);
% 
% %TH and PH
% PH2 = atan2(V2,U2)*180/pi;
% TH2 = asind(V2./sind(PH2));
% %issue when PH = 0, then 
% index = isnan(TH2);%
% TH2(index) = abs(AZ(index));
% TH-TH2

%Compute FF --------------
K = 2*pi/lambda;
m = -MI/2:1:(MI/2-1);
n = -NI/2:1:(NI/2-1); %from balanis
[M,N] = meshgrid(m,n);
Kx = 2*K*M/MI;
Ky = 2*K*N/NI;
Kz = sqrt(K^2 - Kx.^2 - Ky.^2);

%DFT -----------
%deltas for trapezoidal approximation NARASIMHAM 1984
f =E_co_cmplx;
deltaE_co = [f(:,2)-f(:,1)  (f(:,3:end) - f(:,1:(end-2)))/2 f(:,end)-f(:,end-1)];
f =E_x_cmplx;
deltaE_x = [f(:,2)-f(:,1)  (f(:,3:end) - f(:,1:(end-2)))/2 f(:,end)-f(:,end-1)];

for j=1:NI %loop for Ky
    for i=1:MI %loop for Kx
        Fco(j,i) = 0;
        dFco(j,i) = 0;
        Fx(j,i) = 0;
        dFx(j,i) = 0;
        for l=1:NI %loop for y
            Fco_row = dx*sum(E_co_cmplx(l,:).*exp(1i*(Kx(j,i)*X(l,:))))...
                *dy*exp(1i*(Ky(j,i)*Y(l,1)));
            dFco_xrow = dx*sum(deltaE_co(l,:).*exp(1i*(Kx(j,i)*X(l,:))))...
                *dy*exp(1i*(Ky(j,i)*Y(l,1)));
            th = (Kx(j,i)/K)*dx/2;
            Fco_TDFT = sinc(th/pi).*Fco_row + 1./(1i*2*th).*(exp(1i.*th)-sinc(th/pi)).*dFco_xrow;
            Fco(j,i) =  Fco(j,i) + Fco_row;%Fco_TDFT;
            
            
            Fx_row = dx*sum(E_x_cmplx(l,:).*exp(1i*(Kx(j,i)*X(l,:))))...
                *dy*exp(1i*(Ky(j,i)*Y(l,1)));
            dFx_xrow = dx*sum(deltaE_x(l,:).*exp(1i*(Kx(j,i)*X(l,:))))...
                *dy*exp(1i*(Ky(j,i)*Y(l,1)));
            th = (Kx(j,i)/K)*dx/2;
            Fx_TDFT = sinc(th/pi).*Fx_row + 1./(1i*2*th).*(exp(1i.*th)-sinc(th/pi)).*dFx_xrow;
            %db(Fco_TDFT)-db(Fco_xrow)
            Fx(j,i) =  Fx(j,i) + Fx_row;%Fx_TDFT;
        end
        %Fco(i,j) = sum(sum(E_co_cmplx.*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
        %Fx(i,j) = sum(sum(E_x_cmplx.*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
        
    end
end

Fco_Interp = interp2(Kx, Ky, Fco,K*U,K*V,'spline');
Fx_Interp = interp2(Kx, Ky, Fx,K*U,K*V,'spline');

FTH = cosd(TH).*(Fco_Interp.*cosd(PH) + Fx_Interp.*sind(PH));
FPH = cosd(TH).*(-Fco_Interp.*sind(PH) + Fx_Interp.*cosd(PH));

[FL3co,FL3x] = ThPh2L3(TH,PH,FTH,FPH);
[FL2Ico,FL2Ix] = ThPh2L2I(TH,PH,FTH,FPH);
[FL2IIco,FL2IIx] = ThPh2L2II(TH,PH,FTH,FPH);
%%
figure
surf(X,Y,(E_co_DB))
shading interp
colorbar
view(2)


figure
surf(X,Y,(E_x_DB))
shading interp
colorbar
view(2)
fft(E_co_cmplx);

figure
imagesc(abs(UVW2ThPh(Kx/K,Ky/K,abs(Kz/K))))

%%

[figHandler] = plotPatterns(AZ,EL,FL3co,FL3x,[0])
[figHandler] = plotPatterns(AZ,EL,FL2Ico,FL2Ix,[0])
[figHandler] = plotPatterns(AZ,EL,FL2IIco,FL2IIx,[0])
%plot an sphere with vectors
figure
pbaspect([1 1 1])
% s =surf(U,V,W)
% s.FaceColor = 'none';
% s.EdgeColor = 'flat'
% axis square
% hold on

[EX,EY,EZ] = ThPh2XYZ(TH,PH,(FTH),(FPH));
quiver3(U,V,W,EX,EY,EZ)
xlabel('x-axis')
ylabel('y-axis')

% %THPH coordinates
% unit = ones(FF.nbr_samples,FF.nbr_samples);
% zero = zeros(FF.nbr_samples,FF.nbr_samples);
% uTH = unit;
% uPH = unit;
% figure
% pbaspect([1 1 1])
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(uTH),(zero));
% quiver3(U,V,W,EX,EY,EZ,'red')
% hold on
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,zero,uPH)
% quiver3(U,V,W,EX,EY,EZ,'blue')
% 
% %L3 coordinates
% unit = ones(FF.nbr_samples,FF.nbr_samples)*5;
% zero = zeros(FF.nbr_samples,FF.nbr_samples);
% uL3co = unit;
% uL3x = unit;
% figure
% pbaspect([1 1 1])
% [uTH,uPH] = L32ThPh(TH,PH,uL3co,zero);
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(uTH),(uPH));
% quiver3(U,V,W,EX,EY,EZ,'red')
% hold on
% [uTH,uPH] = L32ThPh(TH,PH,zero,uL3x);
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(uTH),(uPH));
% quiver3(U,V,W,EX,EY,EZ,'blue')


% %L3 Watch in componentes
% figure
% [ETHfromL3,EPHfromL3] = L32ThPh(TH,PH,EL3co,zeros(FF.nbr_samples,FF.nbr_samples));
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(ETHfromL3),(EPHfromL3));
% quiver3(U,V,W,EX,EY,EZ)
% figure
% [ETHfromL3,EPHfromL3] = L32ThPh(TH,PH,zeros(FF.nbr_samples,FF.nbr_samples),EL3x);
% [EX,EY,EZ] = ThPh2XYZ(TH,PH,(ETHfromL3),(EPHfromL3));
% quiver3(U,V,W,EX,EY,EZ)

%% Import FF data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/FFL2AzElCalUnif67.txt';
dataFF = importFFfromfile(filename);
AZ_meas=dataFF(:,:,1);
EL_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
F_co_cmplx_file = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
F_x_cmplx_file = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);



%% Compare
%plot FF imported
[figHandler] = plotPatterns(AZ,EL,F_co_cmplx_file,F_x_cmplx_file,[0])



%OVERLAP PLOTS
    %get norm (MAX) val
    Pattern2plot_Co = F_co_cmplx_file;
    Pattern2plot_X  = F_x_cmplx_file;
    normComputed = max(max(FL2Ico(EL==0&AZ==0)./F_co_cmplx_probe(EL_meas==0&AZ_meas==0)));%./F_co_cmplx_probe));
    F_array_Computed_Co = FL2Ico./F_co_cmplx_probe/normComputed;%./F_co_cmplx_probe
    F_array_Computed_X  = FL2Ix/normComputed;
    
    plot2DCOandX(AZ,EL,Pattern2plot_Co,Pattern2plot_X,F_array_Computed_Co,F_array_Computed_X)    
    
    figH_Cuts = figure
    norm = max(max(Pattern2plot_Co(EL==0&AZ==0)));
    index2norm = Pattern2plot_Co==norm;
    
    
    %fetch for cut angles
    options = [0 30];%-45:5:45;
    normElAngle = EL_meas(index2norm);
    normAzAngle = AZ_meas(index2norm);
    [angleErrorEl, index2ElAngle] = min(abs(options-normElAngle));
    [angleErrorAz, index2AzAngle] = min(abs(options-normAzAngle));
    targetElAngle = options(index2ElAngle);
    targetAzAngle = options(index2AzAngle);
    
    %AZ cut
    [~, index2EL]= min(abs(EL_meas(:)-targetElAngle));
    index2plot  = EL_meas == EL_meas(index2EL(1));
    [~, index2EL]= min(abs(EL(:)-targetElAngle));
    index2plot_emb  = EL == EL(index2EL(1));
    ax1=subplot(2,1,1);
    hold on
    plot(AZ_meas(index2plot),db(Pattern2plot_Co(index2plot)/norm),'k', ...
        'linewidth',linewidth)
    ax1.ColorOrderIndex =5;
    plot(AZ(index2plot_emb),db(F_array_Computed_Co(index2plot_emb)), ...
        'linewidth',2)
    plot(AZ_meas(index2plot),db(Pattern2plot_X(index2plot)/norm),'k--', ...
        'linewidth',linewidth)
    ax1.ColorOrderIndex =5;
    plot(AZ(index2plot_emb),db(F_array_Computed_X(index2plot_emb)),'--', ...
        'linewidth',2)
    xlim([-65 65])
    ylim([-50 0])
    xlabel('Azimuth (deg)')
    ylabel('Amplitude (dB)')
    grid on
    grid minor
    legend('Measured','Computed','location','southeast')
    %axis square
    %ylim([-65 65])
    
    %EL cut
    [~, index2Az]= min(abs(AZ_meas(:)-targetAzAngle));
    index2plot  = AZ_meas == AZ_meas(index2Az(1));
    [~, index2Az]= min(abs(AZ(:)-targetAzAngle));
    index2plot_emb  = AZ == AZ(index2Az(1));
    ax2=subplot(2,1,2);
    %set(h1, 'Ydir', 'reverse')
    %set(h1, 'YAxisLocation', 'Right')
    hold on
    plot(EL_meas(index2plot),db(Pattern2plot_Co(index2plot)/norm),'k', ...
        'linewidth',linewidth)
    ax2.ColorOrderIndex =5;
    plot(EL(index2plot_emb),db(F_array_Computed_Co(index2plot_emb)), ...
        'linewidth',2)
    plot(EL_meas(index2plot),db(Pattern2plot_X(index2plot)/norm),'k--', ...
        'linewidth',linewidth)
    ax2.ColorOrderIndex =5;
    plot(EL(index2plot_emb),db(F_array_Computed_X(index2plot_emb)),'--', ...
        'linewidth',2)
    xlim([-65 65])
    ylim([-50 0])
    xlabel('Elevation (deg)')
    ylabel('Amplitude (dB)')
    grid on
    grid minor
    %axis square
    
%     strFileName2D = strcat(strFolderPath,'2DPatternID',num2str(n),...
%         'Az',num2str(targetAzAngle),...
%         'El',num2str(targetElAngle),...
%         'Pol',pol(n));
%     strFileNameCuts = strcat(strFolderPath,'CutsPattern',num2str(n),...
%         'Az',num2str(targetAzAngle),...
%         'El',num2str(targetElAngle),...
%         'Pol',pol(n));
    %saveas(figH_2D,strFileName2D,'png')
    %saveas(figH_Cuts,strFileNameCuts,'png')

