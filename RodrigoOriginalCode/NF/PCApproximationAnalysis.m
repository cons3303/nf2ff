%% Effect of approximations in PC
%load a measured pattern
%load probe patterns
%simulate a NF scan by dot(AUT,Probe)
%describe the x-pol level of the result in function of the approximation

clc
clear all
close all
set(0,'defaultAxesFontSize',17)
set(0,'defaultAxesFontName','Times')
set(0,'defaulttextInterpreter','latex') 

%% Input parameters
% FF grid definition
%nbr of samples
param.freq = 5.35e9;
linewidth = 2;
FF.nbr_samples = 130;
FF.H_angle_range = 130;
FF.V_angle_range = 130;

%% Sampling coordinates
%create grid of FF sample angles --------------
H_angles = linspace(-FF.H_angle_range/2,FF.H_angle_range/2,FF.nbr_samples);
V_angles = linspace(-FF.V_angle_range/2,FF.V_angle_range/2,FF.nbr_samples);

[AZ,EL] = meshgrid(H_angles,V_angles);

[U,V,W] = AzEl2UVW(AZ,EL);
[TH,PH] = UVW2ThPh(U,V,W);

%% load AUT pattern
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif67 data/FFL3PCCalUnif67.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
%identify as H pol
pol=1; %1-Hpol / 0-Vpol

dataFF = importFFfromfile(filename,1);
AZ_meas=dataFF(:,:,1);
EL_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
F_co_PC_meas = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
F_x_PC_meas = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);

%convert from co and x to H,V or Az,El
if pol
    E_H_L3_meas = interp2(AZ_meas, EL_meas, F_co_PC_meas,AZ,EL,'spline');
    E_V_L3_meas = interp2(AZ_meas, EL_meas, F_x_PC_meas,AZ,EL,'spline');
else
    E_V_L3_meas = interp2(AZ_meas, EL_meas, F_co_PC_meas,AZ,EL,'spline');
    E_H_L3_meas = interp2(AZ_meas, EL_meas, F_x_PC_meas,AZ,EL,'spline');
end
%INTO OTHER DEFINITIONS
%LUDWIG3
E_H_L3 = E_H_L3_meas;
E_V_L3 = E_V_L3_meas;
%to TH PH
[E_TH,E_PH] = L32ThPh(TH,PH,E_H_L3_meas,E_V_L3_meas);
%to Ludwig2I (AZ/EL)definition
[E_AZ_L2I,E_EL_L2I] = ThPh2L2I(TH,PH,E_TH,E_PH);



%% loat Probe pattern
[E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,E_L2I_AZ_ProbeEy,E_L2I_EL_ProbeEy]...
    = getWR187Patterns(TH,PH,0);

%% Process probe effect
% 
% for i=1:15
% %Added offset for X-pol of the AUT
% offset(i) = db2mag(5*(i-1)-40);
% %dot product 
% %taken with probe Ex
% DEx =  E_AZ_L2I.*E_L2I_AZ_ProbeEx + E_EL_L2I.*E_L2I_EL_ProbeEx*offset(i); 
%  %taken with probe Ey
% DEy = E_AZ_L2I.*E_L2I_AZ_ProbeEy*offset(i) + E_EL_L2I.*E_L2I_EL_ProbeEy;
% 
% % Probe correction - Approximated
% E_AZ_L2I_PC(:,:,i) = DEx./E_L2I_AZ_ProbeEx;
% E_EL_L2I_PC(:,:,i) = DEy./E_L2I_EL_ProbeEy;
% end
% 
% 
% %% plot 
% figure
% max_x= max((db(E_EL_L2I_PC(65,:,:))))
% plot(db(offset),squeeze(max_x),'*-')

%%
for i=1:16
    %Added offset for X-pol of the AUT
        offset(i) = db2mag(5*(i-1)-40);
    for j=1:15
        %Added offset for X-pol of the Probe
        offsetProbe(j) = db2mag(5*(j-1)-40);
        %dot product
        %taken with probe Ex
        DEx =  E_AZ_L2I.*E_L2I_AZ_ProbeEx + E_EL_L2I.*E_L2I_EL_ProbeEx*offset(i)*offsetProbe(j);
        %taken with probe Ey
        DEy = E_AZ_L2I.*E_L2I_AZ_ProbeEy*offsetProbe(j) + E_EL_L2I.*E_L2I_EL_ProbeEy*offset(i);
        
        % Probe correction - Approximated
        E_AZ_L2I_PC(:,:,i,j) = DEx./E_L2I_AZ_ProbeEx;
        E_EL_L2I_PC(:,:,i,j) = DEy./E_L2I_EL_ProbeEy;
    end
end


%% error in DB

ratioEy = db2mag([0:-5:-80]);
ratioAUT = db2mag([0:-5:-60]);

for i=1:numel(ratioEy)
    for j=1:numel(ratioAUT)
        errordB(i,j) = 20*log10(abs(1+ratioEy(i)/ratioAUT(j)))
    end
end
%% plot 
max_x_meas = max(db(E_EL_L2I(65,:)));

figure
max_x= max((db(E_EL_L2I_PC(65,:,:,:))))
plot(db(offset),squeeze(max_x),'*-')

figure
offset_x_AUT_matrix = repmat(transp(db(offset)),size(offsetProbe));
imagesc(db(offsetProbe),db(offset),squeeze(max_x)-(max_x_meas+offset_x_AUT_matrix))
ylabel('offset x-pol AUT')
xlabel('offset x-pol Probe')
colorbar

%Ratios
figure
surf(AZ,EL,db(E_L2I_AZ_ProbeEy./E_L2I_EL_ProbeEy))
shading interp
view(2)
colorbar
title('Ratio $\frac{E^{Ey}_{cross}}{E^{Ey}_{co}}$')
figure
surf(AZ,EL,db(E_EL_L2I./E_AZ_L2I))
shading interp
view(2)
colorbar
title('Ratio $\frac{E^{AUT}_[cross}}{E^{AUT}_{co}}$')

ratio = (E_L2I_AZ_ProbeEy./E_L2I_EL_ProbeEy)./...
        (E_EL_L2I./E_AZ_L2I);

figure
hold on
subplot(1,2,1)
surf(AZ,EL,20*log10(abs(1+ratio)))
shading interp
view(2)
colorbar
axis square
title('error')


subplot(1,2,2)
surf(AZ,EL,20*log10(abs(1+ratio)))
shading interp
view(2)
limits=7
zlim([-limits limits])
caxis([-limits limits])
colorbar
axis square
title('error')



figure
[ratioX,ratioY] = meshgrid(db(ratioAUT),db(ratioEy));
surf(ratioX,ratioY,errordB)
shading interp
view(2)
colorbar
xlabel('AUT x/co')
ylabel('Probe x/co')
title('Error $e_{dB}$')


% %for plotting --------------
% if pol == 0 %Vertical pol
%     F_meas_co = E_EL_L2I;
%     F_meas_cross = E_AZ_L2I;
%     F_comp_co = E_EL_L2I_PC;
%     F_comp_cross = E_AZ_L2I_PC;
% else
%     F_meas_co = E_AZ_L2I;
%     F_meas_cross = E_EL_L2I;
%     F_comp_co = E_AZ_L2I_PC;
%     F_comp_cross = E_EL_L2I_PC;
% end
% X2plot_meas = AZ_meas;
% Y2plot_meas = EL_meas;
% X2plot_comp = AZ;
% Y2plot_comp = EL;
% %plot parameters
% ymin = -60;
% ymax = 0;
% Xcut = 0;%45.5; %where to cut the data
% Ycut = 0;%39; %where to cut the data
% 
% %Plotting --------- no user input needed
% % % %     plot2DCOandX(AZ,EL,F_meas_co,F_meas_cross,F_comp_co,F_comp_cross) 
% % % %     suptitle('Ludwig 3, Probe Corrected - Computed from NF data')
% %norms
% norm_meas = max(max(F_meas_co));
% norm_Interp = max(max(F_comp_co));
% 
% %Cut in Kx/K or U
% figure
% subplot(2,1,1)
% hold on
% indxs = Y2plot_meas==Ycut;
% plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
% indxs = Y2plot_comp==Ycut;
% plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
% plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
% plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
% ylim([ymin ymax])
% xlabel('Azimuth')
% ylabel('Magnitude(dB)')
% legend('L3 meas','L3 from NF')
% grid on
% grid minor
% %Cut in Ky/K or V
% subplot(2,1,2)
% hold on
% indxs = X2plot_meas==Xcut;
% plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
% plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
% indxs = X2plot_comp==Xcut;
% plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
% plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
% ylim([ymin ymax])
% xlabel('Elevation')
% ylabel('Magnitude(dB)')
% grid on
% grid minor
