
function [data] = importFFfromfile(filename,mode)
%% Get Co and X from a NSI data file
%mode =1-AzEl(default)/2 - thph/3- Kx/Ky
%data = X,Y,AmpPol1,PhasePol1,AmpPol2,PhasePol2
% For more information, see the TEXTSCAN documentation.
% Ex. filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NFCalUnif10.txt';
%     if mode == 1
%         line2find = 'Azimuth (deg)  Elevation (deg)    Amp      Phase   ';
%     elseif mode == 2
%         line2find = 'Theta (deg)  Phi (deg)    Amp      Phase   ';
%     elseif mode == 3
%         line2find = 'Kx (cycles/L)  Ky (cycles/L)    Amp      Phase   ';
%     end
formatSpec = '%f %f %f %f';

%% Open the text file.
fileID = fopen(filename,'r');
 while ~feof(fileID)         
     
    tline = fgetl(fileID);
    
    %look for the first pol
    line2find = 'Probe-1: Lin-';
    IndexPol = strfind(tline,line2find);
    if ~isempty(IndexPol)
        pol = tline(IndexPol+length(line2find))=='0'; %0-V(Ey)/%1-H(Ex)
    end
    
    %look for data
    if mode == 1
        line2find = 'Azimuth (deg)  Elevation (deg)    Amp      Phase   ';
    elseif mode == 2
        line2find = 'Theta (deg)  Phi (deg)    Amp      Phase   ';
    elseif mode == 3
        line2find = 'Kx (cycles/L)  Ky (cycles/L)    Amp      Phase   ';
    end
    IndexC = strfind(tline,line2find);
    if ~isempty(IndexC)
        currentPol = tline(IndexC+length(line2find));
        if currentPol == 'P'
            SData = textscan(fileID,formatSpec,'HeaderLines',0);
            SData = cell2mat(SData);
            [nbr_row nbr_col] = size(SData);
            nbr_samples = sqrt(nbr_row); %samples along x and y
            %arrange in a 3D matrix
            for i=1:nbr_col
                data(:,:,i) = transp(reshape(SData(:,i),nbr_samples,nbr_samples));
            end
        end
        if currentPol == 'X'
            SData2 = textscan(fileID,formatSpec,'HeaderLines',0);
            Xpolpart = cell2mat(SData2(3:4)); %just get the important part
            [nbr_row nbr_col] = size(Xpolpart);
            nbr_samples = sqrt(nbr_row); %samples along x and y
            %arrange in a 3D matrix
            for i=5:6
                data(:,:,i) = transp(reshape(Xpolpart(:,i-4),nbr_samples,nbr_samples));
            end
       end
    end
 end
 

 
%rawdata = importdata(filename,' ',49);
fclose(fileID)
clearvars -except 'data'