%% IXR tests
%load probe patterns, bc i have both pols and co and x data
%calculate the IXR at all angles

clc
clear all
close all
set(0,'defaultAxesFontSize',17)
set(0,'defaultAxesFontName','Times')
set(0,'defaulttextInterpreter','latex') 

%% Input parameters
% FF grid definition
%nbr of samples
param.freq = 5.35e9;
linewidth = 2;
FF.nbr_samples = 130;
FF.H_angle_range = 130;
FF.V_angle_range = 130;

%% Sampling coordinates
%create grid of FF sample angles --------------
H_angles = linspace(-FF.H_angle_range/2,FF.H_angle_range/2,FF.nbr_samples);
V_angles = linspace(-FF.V_angle_range/2,FF.V_angle_range/2,FF.nbr_samples);

[AZ,EL] = meshgrid(H_angles,V_angles);

[U,V,W] = AzEl2UVW(AZ,EL);
[TH,PH] = UVW2ThPh(U,V,W);

%% load Probe pattern
[E_L2I_AZ_ProbeEx,E_L2I_EL_ProbeEx,E_L2I_AZ_ProbeEy,E_L2I_EL_ProbeEy]...
    = getWR187Patterns(TH,PH,0);

%% Calculate IXR
% build the jones matrix for all directions
Jones(1,1,:)= E_L2I_AZ_ProbeEx(:);
Jones(1,2,:)= E_L2I_EL_ProbeEx(:);
Jones(2,1,:)= E_L2I_AZ_ProbeEy(:);
Jones(2,2,:)= E_L2I_EL_ProbeEy(:);

     
%get the condition-2 k
for i=1:numel(E_L2I_AZ_ProbeEx(:))
    if sum(sum(isnan(Jones(:,:,i))))
        k2(i)=2;
    else
        k2(i)=cond(Jones(:,:,i));
    end
end

%Calculate the IXR
IXR = ((k2+1)./(k2-1)).^2;
%rearrange
IXR =reshape(transp(IXR),size(E_L2I_AZ_ProbeEx));

%% plot
figure
surf(AZ,EL,db(IXR))
shading interp
view(2)
colorbar
%caxis([0 200])

