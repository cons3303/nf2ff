%% NF-FF transformation
clc
clear all
close all
set(0,'defaultAxesFontSize',17)
set(0,'defaultAxesFontName','Times')
%% Import NF data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif67 data/NFCalUnif67.txt';%Hpol CalUnif67 data
data = importNFfromfile(filename);
X=data(:,:,1)*0.0254;
Y=data(:,:,2)*0.0254;

NF_co_DB = data(:,:,3);
NF_co_Deg = data(:,:,4);
NF_co_cmplx = db2mag(NF_co_DB).*exp(1i*NF_co_Deg*pi/180);

NF_x_DB = data(:,:,5);
NF_x_Deg = data(:,:,6);
NF_x_cmplx = db2mag(NF_x_DB).*exp(1i*NF_x_Deg*pi/180);

% Import Probe data
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/ProbeEx/FFL2AzElPCProbeEx.txt';
dataFF = importFFfromfile(filename,1);
AZ_probe=dataFF(:,:,1);
EL_probe=dataFF(:,:,2);

F_co_DB_probe = dataFF(:,:,3);
F_co_Deg_probe = dataFF(:,:,4);
F_co_cmplx_probe = db2mag(F_co_DB_probe).*exp(1i*F_co_Deg_probe*pi/180);

F_x_DB_probe = dataFF(:,:,5);
F_x_Deg_probe = dataFF(:,:,6);
F_x_cmplx_probe = db2mag(F_x_DB_probe).*exp(1i*F_x_Deg_probe*pi/180);

%% Input parameters
% FF grid definition
%nbr of samples
param.freq = 5.35e9;
linewidth = 2;
FF.nbr_samples = 361;
FF.H_angle_range = 130;
FF.V_angle_range = 130;

%% Simple derivations
lambda = 3e8/param.freq;
[Nx, My] = size(X);
NI = 128; %nbr freqs in y
MI = 128; %nbr freqs in x
dx = abs(X(1,2)-X(1,1));
dy = abs(Y(2,1)-Y(1,1));


%% Process NF2FF


%create grid of FF sample angles --------------
H_angles = linspace(-FF.H_angle_range/2,FF.H_angle_range/2,FF.nbr_samples);
V_angles = linspace(-FF.V_angle_range/2,FF.V_angle_range/2,FF.nbr_samples);

[AZ,EL] = meshgrid(H_angles,V_angles);

[U,V,W] = AzEl2UVW(AZ,EL);
[TH,PH] = UVW2ThPh(U,V,W);



%Compute FF --------------
K = 2*pi/lambda;
m = -MI/2:1:(MI/2-1);
n = -NI/2:1:(NI/2-1); %from balanis
[M,N] = meshgrid(m,n);
Kx = 2*K*M/MI;
Ky = 2*K*N/NI;
Kz = sqrt(K^2 - Kx.^2 - Ky.^2);
%for interpolation on desired FF coordinates
Kx_Interp = K*U;
Ky_Interp = K*V;
Kz_Interp = K*W;

%DFT -----------
%deltas for trapezoidal approximation NARASIMHAM 1984

for i=1:NI
    for j=1:MI
        Fx(i,j) = sum(sum(NF_co_cmplx.*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
        Fy(i,j) = sum(sum(NF_x_cmplx .*exp(1i*(Kx(i,j)*X+Ky(i,j)*Y))*dx*dy));
    end
end

%Fx = exp(1i*Kz*0.0277*6).*Fx;
%Fy = exp(1i*Kz*0.0277*6).*Fy;

%FFT ------------
%Fco= ifftshift(ifft2(NF_co_cmplx,NI,MI));
%Fx = ifftshift(ifft2(NF_x_cmplx,NI,MI));




Fx_Interp = interp2(Kx, Ky, Fx,Kx_Interp,Ky_Interp,'spline');
Fy_Interp = interp2(Kx, Ky, Fy,Kx_Interp,Ky_Interp,'spline');
Fz_Interp = -(Fx_Interp.*Kx_Interp + Fy_Interp.*Ky_Interp)./Kz_Interp;
%cosTH correction - due to the approximation of Asymptotic evaluation
Ex = cosd(TH).*Fx_Interp;
Ey = cosd(TH).*Fy_Interp;
Ez = cosd(TH).*Fz_Interp;

%cartesian to spherical
% FTH = (Fx_Interp.*cosd(TH).*cosd(PH) + Fy_Interp.*cosd(TH).*sind(PH) -Fz_Interp.*sind(TH));
% FPH = (-Fx_Interp.*sind(PH) + Fy_Interp.*cosd(PH));
%FTH = (Fx_Interp.*cosd(PH) + Fy_Interp.*sind(PH))./cosd(TH);
%FPH = (-Fx_Interp.*sind(PH) + Fy_Interp.*cosd(PH));
[FTH,FPH] = XYZ2ThPh(TH,PH,Ex,Ey,Ez);


[FL3co,FL3x] = ThPh2L3(TH,PH,FTH,FPH);
[FL2Ico,FL2Ix] = ThPh2L2I(TH,PH,FTH,FPH);
[FL2IIco,FL2IIx] = ThPh2L2II(TH,PH,FTH,FPH);


%%
figure
surf(X,Y,(NF_co_DB))
shading interp
colorbar
view(2)
title('NF Data - Co Pol')

figure
surf(X,Y,(NF_x_DB))
shading interp
colorbar
view(2)
title('NF Data - Cross Pol')



%% Import FF data - NO PROBE CORRECTION  - L2 Az/EL (given in Kx and Ky)
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif67 data/FFKxKyNOPCCalUnif67.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,3);
U_meas=dataFF(:,:,1);
V_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
E_AZ_NOPC_meas = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
E_EL_NOPC_meas = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);

E_AZ_NOPC_meas_Interp = interp2(U_meas, V_meas, E_AZ_NOPC_meas,U,V,'spline');
E_EL_NOPC_meas_Interp = interp2(U_meas, V_meas, E_EL_NOPC_meas,U,V,'spline');

%for plotting --------------
F_meas_co = E_AZ_NOPC_meas_Interp;
F_meas_cross = E_EL_NOPC_meas_Interp;
F_comp_co = Ex;
F_comp_cross = Ey;
X2plot_meas = U;
Y2plot_meas = V;
X2plot_comp = U;
Y2plot_comp = V;
%plot parameters
ymin = -60;
ymax = 0;
Xcut = 0; %where to cut the data
Ycut = 0; %where to cut the data

%Plotting --------- no user input needed
%norms
norm_meas = max(max(F_meas_co));
norm_comp = max(max(F_comp_co));
%2d plotting
plot2DCOandX(AZ,EL,F_meas_co,F_meas_cross,F_comp_co,F_comp_cross)
figure
surf(X2plot_meas,Y2plot_meas,db(F_meas_co./norm_meas))
shading interp
colorbar
zlim([-130 0])
view(2)
title('NO PC - L2I')
figure
surf(X2plot_comp,Y2plot_comp,db(F_comp_co./norm_comp))
shading interp
colorbar
zlim([-130 0])
view(2)
title('What i call Fx')
%Cut in Kx/K or U
figure
subplot(2,1,1)
hold on
indxs = Y2plot_meas==Ycut;
plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
indxs = Y2plot_comp==Ycut;
plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_comp),'g','linewidth',linewidth)
plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_comp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Kx/K')
ylabel('Magnitude(dB)')
%plot phase
%Cut in Ky/K or V
subplot(2,1,2)
hold on
indxs = X2plot_meas==Xcut;
plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
indxs = X2plot_comp==Xcut;
plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_comp),'g','linewidth',linewidth)
plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_comp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Ky/K')
ylabel('Magnitude(dB)')
suptitle('Angular spectrum - NO PC ')

% PLOT PHASES -------------------------
%Cut in Kx/K or U
figure
subplot(2,1,1)
hold on
indxs = Y2plot_meas==Ycut;
plot(X2plot_meas(indxs),angle(F_meas_co(indxs)/norm_meas)*180/pi,'k')
plot(X2plot_meas(indxs),angle(F_meas_cross(indxs)/norm_meas)*180/pi,'k--')
indxs = Y2plot_comp==Ycut;
plot(X2plot_comp(indxs),angle(F_comp_co(indxs)/norm_comp)*180/pi,'g','linewidth',linewidth)
plot(X2plot_comp(indxs),angle(F_comp_cross(indxs)/norm_comp)*180/pi,'g--','linewidth',linewidth)
%ylim([ymin ymax])
xlabel('Kx/K')
ylabel('PHASE(deg)')

%Cut in Ky/K or V
subplot(2,1,2)
hold on
indxs = X2plot_meas==Xcut;
plot(Y2plot_meas(indxs),angle(F_meas_co(indxs)/norm_meas)*180/pi,'k')
plot(Y2plot_meas(indxs),angle(F_meas_cross(indxs)/norm_meas)*180/pi,'k--')
indxs = X2plot_comp==Xcut;
plot(Y2plot_comp(indxs),angle(F_comp_co(indxs)/norm_comp)*180/pi,'g','linewidth',linewidth)
plot(Y2plot_comp(indxs),angle(F_comp_cross(indxs)/norm_comp)*180/pi,'g--','linewidth',linewidth)
%ylim([ymin ymax])
xlabel('Ky/K')
ylabel('PHASE(deg)')
suptitle('Phase of the Angular spectrum - NO PC ')

%Prove that what i exported as L2 AZ/EL is the same as what I obtained from
%fourier transforms


figure
surf(AZ,EL,db(((F_meas_co./norm_meas)-(F_comp_co./norm_comp))))
shading interp
view(2)
colorbar
title('Difference between Exported L2 AZ/EL data and my fourier transform')
% % % 
% % % %% Import Kx KY FF data - WITh PROBE CORRECTION
% % % filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif67 data/FFKxKyPCCalUnif67.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
% % % dataFF = importFFfromfile(filename,3);
% % % U_meas=dataFF(:,:,1);
% % % V_meas=dataFF(:,:,2);
% % % 
% % % F_co_DB_file = dataFF(:,:,3);
% % % F_co_Deg_file = dataFF(:,:,4);
% % % F_co_PC_meas = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);
% % % 
% % % F_x_DB_file = dataFF(:,:,5);
% % % F_x_Deg_file = dataFF(:,:,6);
% % % F_x_PC_meas = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);
% % % 
% % % F_L2I_co_PC_Interp = interp2(U_meas, V_meas, F_co_PC_meas,U,V,'spline');
% % % F_L2I_x_PC_Interp = interp2(U_meas, V_meas, F_x_PC_meas,U,V,'spline');
% % % 
% % % %for plotting --------------
% % % F_meas_co = F_L2I_co_PC_Interp;
% % % F_meas_cross = F_L2I_x_PC_Interp;
% % % F_comp_co = Fx_Interp;
% % % F_comp_cross = Fy_Interp;
% % % X2plot_meas = U;%U_meas;
% % % Y2plot_meas = V;%V_meas;
% % % X2plot_comp = U;
% % % Y2plot_comp = V;
% % % %plot parameters
% % % ymin = -60;
% % % ymax = 0;
% % % Xcut = U(181,181);%0; %where to cut the data
% % % Ycut = V(181,181); %where to cut the data
% % % 
% % % %Plotting --------- no user input needed
% % % %norms
% % % norm_meas = max(max(F_meas_co));
% % % norm_Interp = max(max(F_comp_co));
% % % %2d plotting
% % % figure
% % % surf(X2plot_meas,Y2plot_meas,db(F_meas_co))
% % % shading interp
% % % colorbar
% % % zlim([-130 -40])
% % % view(2)
% % % %Cut in Kx/K or U
% % % figure
% % % subplot(2,1,1)
% % % hold on
% % % indxs = Y2plot_meas==Ycut;
% % % plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
% % % plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
% % % indxs = Y2plot_comp==Ycut;
% % % plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
% % % plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
% % % ylim([ymin ymax])
% % % xlabel('Kx/K')
% % % ylabel('Magnitude(dB)')
% % % 
% % % %Cut in Ky/K or V
% % % subplot(2,1,2)
% % % hold on
% % % indxs = X2plot_meas==Xcut;
% % % plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
% % % plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
% % % indxs = X2plot_comp==Xcut;
% % % plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
% % % plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
% % % ylim([ymin ymax])
% % % xlabel('Ky/K')
% % % ylabel('Magnitude(dB)')
% % % suptitle('Angular spectrum - PC ')
% % % 
% % % % Transform Kx,Ky angular spectrum data to a pattern
% % % FL2Ico_Interp = interp2(K*U_meas, K*V_meas, F_co_PC_meas,Kx_Interp,Ky_Interp,'spline');%cosd(AZ).*
% % % FL2Ix_Interp = interp2(K*U_meas, K*V_meas, F_x_PC_meas,Kx_Interp,Ky_Interp,'spline');%cosd(EL).





%% Import L2I FF data - WITHOUT PROBE CORRECTION  - L2 Az/EL (given in AZ and EL)
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif67 data/FFL2AzElNOPCCalUnif67.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,1);
AZ_meas=dataFF(:,:,1);
EL_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
F_L2I_co_NOPC_meas = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
F_L2I_x_NOPC_meas = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);

F_L2I_co_NOPC_Interp = interp2(AZ_meas, EL_meas, F_L2I_co_NOPC_meas,AZ,EL,'spline');
F_L2I_x_NOPC_Interp = interp2(AZ_meas, EL_meas, F_L2I_x_NOPC_meas,AZ,EL,'spline');
%for plotting --------------
F_meas_co = F_L2I_co_NOPC_Interp;
F_meas_cross = F_L2I_x_NOPC_Interp;
F_comp_co = FL2Ico;
F_comp_cross = FL2Ix;
X2plot_meas = AZ_meas;
Y2plot_meas = EL_meas;
X2plot_comp = AZ;
Y2plot_comp = EL;
%plot parameters
ymin = -60;
ymax = 0;
Xcut = 0;%45.5; %where to cut the data
Ycut = 0;%39; %where to cut the data

%Plotting --------- no user input needed
    plot2DCOandX(AZ,EL,F_meas_co,F_meas_cross,F_comp_co,F_comp_cross) 
%norms
norm_meas = max(max(F_meas_co));
norm_Interp = max(max(F_comp_co));
%2d plotting
figure
surf(X2plot_meas,Y2plot_meas,db(F_meas_co))
shading interp
colorbar
zlim([-130 -40])
view(2)
%Cut in Kx/K or U
figure
subplot(2,1,1)
hold on
indxs = Y2plot_meas==Ycut;
plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
indxs = Y2plot_comp==Ycut;
plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Azimuth')
ylabel('Magnitude(dB)')
legend('L2I meas','L2I from NF')
grid on
grid minor
%Cut in Ky/K or V
subplot(2,1,2)
hold on
indxs = X2plot_meas==Xcut;
plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
indxs = X2plot_comp==Xcut;
plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Elevation')
ylabel('Magnitude(dB)')
grid on
grid minor
suptitle('L2I FF - NO PC ')

%% Import FF probe data Ex
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/ProbeEx/FFL3PCProbeEx.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,3);
U_meas=dataFF(:,:,1);
V_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
F_co_ProbeEx = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
F_x_ProbeEx = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);

F_L3_co_ProbeEx_Interp = interp2(U_meas, V_meas, F_co_ProbeEx,U,V,'spline');
F_L3_x_ProbeEx_Interp = interp2(U_meas, V_meas, F_x_ProbeEx,U,V,'spline');



%for plotting --------------
F_meas_co = F_L3_co_ProbeEx_Interp;
F_meas_cross = F_L3_x_ProbeEx_Interp;
F_comp_co = F_L3_x_ProbeEx_Interp;
F_comp_cross = F_L3_x_ProbeEx_Interp;
X2plot_meas = AZ;
Y2plot_meas = EL;
X2plot_comp = AZ;
Y2plot_comp = EL;
%plot parameters
ymin = -60;
ymax = 0;
Xcut = 0; %where to cut the data
Ycut = 0; %where to cut the data

%norms
norm_meas = max(max(F_meas_co));
norm_Interp = max(max(F_comp_co));
%Cut in Kx/K or U
figure
subplot(2,1,1)
hold on
indxs = Y2plot_meas==Ycut;
plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
indxs = Y2plot_comp==Ycut;
plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Azimuth')
ylabel('Magnitude(dB)')
legend('L3 meas','L3 from NF')
grid on
grid minor
%Cut in Ky/K or V
subplot(2,1,2)
hold on
indxs = X2plot_meas==Xcut;
plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
indxs = X2plot_comp==Xcut;
plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Elevation')
ylabel('Magnitude(dB)')
grid on
grid minor
suptitle('L3 FF - Probe Ex')

%Tranform to Fx and Fy

[E_TH_probeEx,E_PH_probeEx]=L32ThPh(TH,PH, F_L3_co_ProbeEx_Interp, F_L3_x_ProbeEx_Interp);
[Ex_probeEx,Ey_probeEx,Ez_probeEx] =ThPh2XYZ(TH,PH, E_TH_probeEx, E_PH_probeEx);
%THE ACTUAL CORRECTION FACTORS
Fx_probeEx = Ex_probeEx./cosd(TH);
Fy_probeEx = Ey_probeEx./cosd(TH);

%plot cartesian components
plotPatterns(AZ,EL,Fx_probeEx,Fy_probeEx,[0])
% figure
% surf(U,V,db(Ex_probeEx))
% shading interp
% view(2)
% xlabel('U')
% ylabel('V')
% title('Ex - Probe Ex')
% %Ey
% figure
% surf(U,V,db(Ey_probeEx))
% shading interp
% view(2)
% xlabel('U')
% ylabel('V')
% title('Ey - Probe Ex')

%% Import FF probe data Ey
filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/ProbeEy/FFL3PCProbeEy.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,3);
U_meas=dataFF(:,:,1);
V_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
F_x_ProbeEy = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
F_co_ProbeEy = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);

F_L3_co_ProbeEy_Interp = interp2(U_meas, V_meas, F_co_ProbeEy,U,V,'spline');
F_L3_x_ProbeEy_Interp = interp2(U_meas, V_meas, F_x_ProbeEy,U,V,'spline');



%for plotting --------------
F_meas_co = F_L3_co_ProbeEy_Interp;
F_meas_cross = F_L3_x_ProbeEy_Interp;
F_comp_co = F_L3_x_ProbeEy_Interp;
F_comp_cross = F_L3_x_ProbeEy_Interp;
X2plot_meas = AZ;
Y2plot_meas = EL;
X2plot_comp = AZ;
Y2plot_comp = EL;
%plot parameters
ymin = -60;
ymax = 0;
Xcut = 0; %where to cut the data
Ycut = 0; %where to cut the data

%norms
norm_meas = max(max(F_meas_co));
norm_Interp = max(max(F_comp_co));
%Cut in Kx/K or U
figure
subplot(2,1,1)
hold on
indxs = Y2plot_meas==Ycut;
plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
indxs = Y2plot_comp==Ycut;
plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Azimuth')
ylabel('Magnitude(dB)')
legend('L3 meas','L3 from NF')
grid on
grid minor
%Cut in Ky/K or V
subplot(2,1,2)
hold on
indxs = X2plot_meas==Xcut;
plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
indxs = X2plot_comp==Xcut;
plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Elevation')
ylabel('Magnitude(dB)')
grid on
grid minor
suptitle('L3 FF - Probe Ey ')

%Tranform to Fx and Fy

[E_TH_probeEy,E_PH_probeEy]=L32ThPh(TH,PH, F_L3_co_ProbeEy_Interp, F_L3_x_ProbeEy_Interp);
[Ex_probeEy,Ey_probeEy,Ez_probeEy] =ThPh2XYZ(TH,PH, E_TH_probeEy, E_PH_probeEy);

% [E_TH_probeEy2,E_PH_probeEy2] = XYZ2ThPh(TH,PH,Ex_probeEy,Ey_probeEy,Ez_probeEy);
% [L3co2,L3x2] = ThPh2L3(TH,PH,E_TH_probeEy2,E_PH_probeEy2);
% L3co2-F_L3_co_ProbeEy_Interp

sum(sum(Ez_probeEy + (Ex_probeEy.*Kx_Interp + Ey_probeEy.*Ky_Interp)./Kz_Interp))
%THE ACTUAL CORRECTION FACTORS
Fx_probeEy = Ex_probeEy./cosd(TH);
Fy_probeEy = Ey_probeEy./cosd(TH);

%plot cartesian components
%Ex
%plot cartesian components
plotPatterns(AZ,EL,Fy_probeEy,Fx_probeEy,[0])
% figure
% surf(U,V,db(Ex_probeEy))
% shading interp
% view(2)
% xlabel('U')
% ylabel('V')
% title('Ex - Probe Ey')
% %Ey
% figure
% surf(U,V,db(Ey_probeEy))
% shading interp
% view(2)
% xlabel('U')
% ylabel('V')
% title('Ey - Probe Ey')

%% Correction
denominator = Fx_probeEx.*Fy_probeEy - Fx_probeEy.*Fy_probeEx;
Fx_corr = (Fx_Interp.*Fy_probeEy - Fy_Interp.*Fy_probeEx)./denominator;
Fy_corr = (-Fx_Interp.*Fx_probeEy + Fy_Interp.*Fx_probeEx)./denominator;
Fz_corr = -(Fx_corr.*Kx_Interp + Fy_corr.*Ky_Interp)./(Kz_Interp);

%cosTH correction - due to the approximation of Asymptotic evaluation
Fx = cosd(TH).*Fx_corr;
Fy = cosd(TH).*Fy_corr;
Fz = cosd(TH).*Fz_corr;

%cartesian to spherical / the cos(TH) at the beginning is the approximation
%FTH_corr = (Fx.*cosd(TH).*cosd(PH) + Fy.*cosd(TH).*sind(PH) -Fz.*sind(TH));
%FPH_corr = (-Fx.*sind(PH) + Fy.*cosd(PH));
%FTH_corr = (Fx.*cosd(PH) + Fy.*sind(PH))./cosd(TH);
%FPH_corr = (-Fx.*sind(PH) + Fy.*cosd(PH));
[FTH_corr,FPH_corr] = XYZ2ThPh(TH,PH,Fx,Fy,Fz);


[FL3co,FL3x] = ThPh2L3(TH,PH,FTH_corr,FPH_corr);
[FL2Ico_corr,FL2Ix_corr] = ThPh2L2I(TH,PH,FTH_corr,FPH_corr);
[FL2IIco,FL2IIx] = ThPh2L2II(TH,PH,FTH_corr,FPH_corr);


%% COMPARE WITH MEASURED DATA - PROBE CORRECTION EVALUATION

filename = '/Users/rodrigolebron/Dropbox/Measurements/NCAR/ARRC Chapter/APAR 8x8/Patterns/1 - Calibrated Uniform/NF/Hpol CalUnif67 data/FFL2AzElPCCalUnif67.txt';%Hpol CalUnif67 data//FFL2ElAzNOPCCalUnif67.txt
dataFF = importFFfromfile(filename,1);
AZ_meas=dataFF(:,:,1);
EL_meas=dataFF(:,:,2);

F_co_DB_file = dataFF(:,:,3);
F_co_Deg_file = dataFF(:,:,4);
F_L2I_co_PC_meas = db2mag(F_co_DB_file).*exp(1i*F_co_Deg_file*pi/180);

F_x_DB_file = dataFF(:,:,5);
F_x_Deg_file = dataFF(:,:,6);
F_L2I_x_PC_meas = db2mag(F_x_DB_file).*exp(1i*F_x_Deg_file*pi/180);


%for plotting --------------
F_meas_co = F_L2I_co_PC_meas;
F_meas_cross = F_L2I_x_PC_meas;
F_comp_co = FL2Ico_corr;
F_comp_cross = FL2Ix_corr;
X2plot_meas = AZ_meas;
Y2plot_meas = EL_meas;
X2plot_comp = AZ;
Y2plot_comp = EL;
%plot parameters
ymin = -60;
ymax = 0;
Xcut = 0;%45.5; %where to cut the data
Ycut = 0;%39; %where to cut the data

%Plotting --------- no user input needed
    plot2DCOandX(AZ,EL,F_meas_co,F_meas_cross,F_comp_co,F_comp_cross) 
%norms
norm_meas = max(max(F_meas_co));
norm_Interp = max(max(F_comp_co));

%Cut in Kx/K or U
figure
subplot(2,1,1)
hold on
indxs = Y2plot_meas==Ycut;
plot(X2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
indxs = Y2plot_comp==Ycut;
plot(X2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(X2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
plot(X2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Azimuth')
ylabel('Magnitude(dB)')
legend('L2I meas','L2I from NF')
grid on
grid minor
%Cut in Ky/K or V
subplot(2,1,2)
hold on
indxs = X2plot_meas==Xcut;
plot(Y2plot_meas(indxs),db(F_meas_co(indxs)/norm_meas),'k')
plot(Y2plot_meas(indxs),db(F_meas_cross(indxs)/norm_meas),'k--')
indxs = X2plot_comp==Xcut;
plot(Y2plot_comp(indxs),db(F_comp_co(indxs)/norm_Interp),'g','linewidth',linewidth)
plot(Y2plot_comp(indxs),db(F_comp_cross(indxs)/norm_Interp),'g--','linewidth',linewidth)
ylim([ymin ymax])
xlabel('Elevation')
ylabel('Magnitude(dB)')
grid on
grid minor
suptitle('L2I FF - With PC ')